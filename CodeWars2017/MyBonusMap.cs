﻿using Com.CodeGame.CodeWars2017.DevKit.CSharpCgdk.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.CodeGame.CodeWars2017.DevKit.CSharpCgdk
{
    public class BonusMap
    {
        public double[,] Table = new double[BonusMapCalculator.MapPointsAmount, BonusMapCalculator.MapPointsAmount];

        public double[,] RealTable { get; internal set; } =
            new double[BonusMapCalculator.MapPointsAmount, BonusMapCalculator.MapPointsAmount];

        public MapType MapType { get; }
        public double Weight { get; set; }
        public bool IsEmpty { get; internal set; }

        public BonusMap(MapType mapType)
        {
            this.MapType = mapType;
        }

        public BonusMap()
        {
        }

        public void Reflect()
        {
            for (int i = 0; i < BonusMapCalculator.MapPointsAmount; i++)
                for (int j = 0; j < BonusMapCalculator.MapPointsAmount; j++)
                {
                    Table[i, j] = -Table[i, j];
                }
        }

        public BonusMap Trim(int power = 1)
        {
            double maxValue = Double.MinValue;
            double minValue = Double.MaxValue;

            //find max value of the map
            for (int i = 0; i < BonusMapCalculator.MapPointsAmount; i++)
                for (int j = 0; j < BonusMapCalculator.MapPointsAmount; j++)
                {
                    if (Table[i, j] > maxValue)
                        maxValue = Table[i, j];
                    if (Table[i, j] < minValue)
                        minValue = Table[i, j];
                }

            if (Math.Abs(minValue - maxValue) < Double.Epsilon)
            {
                MyStrategy.Universe.Print("Map is empty");
                IsEmpty = true;
                return this;
            }

            //scale map to range [0, 1]
            for (int i = 0; i < BonusMapCalculator.MapPointsAmount; i++)
                for (int j = 0; j < BonusMapCalculator.MapPointsAmount; j++)
                {
                    Table[i, j] = Math.Pow((Table[i, j] - minValue) / (maxValue - minValue), power);

                    if (Table[i, j] > 1 || Table[i, j] < 0 || Double.IsNaN(Table[i, j]))
                        throw new Exception("Wrong map trim.");
                }
            return this;
        }

        public void AddUnitCalculation(Vehicle unit, double maxValueDistance, double maxValue, double zeroValueDistance) =>
            AddUnitCalculation(new AbsolutePosition(unit.X, unit.Y), maxValueDistance, maxValue, zeroValueDistance);

        public void AddUnitCalculation(AbsolutePosition unitPosition, double maxValueDistance, double maxValue, double zeroValueDistance)
        {
            if (maxValueDistance > zeroValueDistance)
            {
                MyStrategy.Universe.Print("Warning! Zero map distance greater then Max value distance!");
                //throw new Exception("Wrong distance limits.");
                zeroValueDistance = maxValueDistance;
            }

            var maxValueDistanceSquared = maxValueDistance * maxValueDistance;
            var zeroValueDistanceSquared = zeroValueDistance * zeroValueDistance;

            for (int i = 0; i < BonusMapCalculator.MapPointsAmount; i++)
                for (int j = 0; j < BonusMapCalculator.MapPointsAmount; j++)
                {
                    var distanceSquared = unitPosition.GetSquaredDistanceToPoint(i * BonusMapCalculator.SizeWorldMapKoeff, j * BonusMapCalculator.SizeWorldMapKoeff);

                    if (distanceSquared <= maxValueDistanceSquared)
                    {
                        if (MapType == MapType.Flat)
                            Table[i, j] = Math.Max(maxValue, Table[i, j]);
                        else
                            Table[i, j] += maxValue;
                    }

                    if (distanceSquared > maxValueDistanceSquared && distanceSquared < zeroValueDistanceSquared)
                    {
                        if (MapType == MapType.Flat)
                            Table[i, j] = Math.Max(maxValue - ((distanceSquared - maxValueDistanceSquared) / zeroValueDistanceSquared), Table[i, j]);
                        else
                        {
                            Table[i, j] += maxValue - ((distanceSquared - maxValueDistanceSquared) / zeroValueDistanceSquared);
                        }

                    }
                }
        }


        public IEnumerable<Tile> GetTileList()
        {
            Trim();
            var tileList = new List<Tile>();
            double tileWidth = BonusMapCalculator.MapCellWidth; //8
            for (int i = 0; i < BonusMapCalculator.MapPointsAmount; i++)
            for (int j = 0; j < BonusMapCalculator.MapPointsAmount; j++)
            {
                var tileCenter = new Point(i * BonusMapCalculator.SizeWorldMapKoeff + tileWidth / 2,
                    j * BonusMapCalculator.SizeWorldMapKoeff + tileWidth / 2);
                tileList.Add(new Tile(tileCenter, tileWidth, Table[i, j], RealTable[i, j]));
                if (Table[i, j] > 1 || Table[i, j] < 0)
                    throw new Exception("Wrong tile trim.");
            }

            return tileList;
        }


        public BonusMap SetWeight(double weigth)
        {
            Weight = weigth;
            return this;
        }
    }

    public interface IBonusMap
    {
        void GenerateMap(MapType type, int weigth, double range);
    }
}
