﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Com.CodeGame.CodeWars2017.DevKit.CSharpCgdk.Model;

namespace Com.CodeGame.CodeWars2017.DevKit.CSharpCgdk
{
    public class DbScan
    {
        private static int eps = 17;
        private static int minPts = 5;
        private HashSet<long> visited = new HashSet<long>();
        private HashSet<long> orphansIds =new HashSet<long>();

        public List<Group> Divide()
        {
            var timer = new Stopwatch();
            timer.Start();

            List<Group> Groups = new List<Group>();
            Divide(Groups, MyStrategy.Universe.MyUnits, minPts);
            Divide(Groups, MyStrategy.Universe.OppUnits, minPts);

            var myOrphans = MyStrategy.Universe.MyUnits.Where(u => orphansIds.Contains(u.Id)).ToList();
            var oppOrphans = MyStrategy.Universe.OppUnits.Where(u => orphansIds.Contains(u.Id)).ToList();
            visited.Clear();
            orphansIds.Clear();

            Divide(Groups, myOrphans, 2);
            Divide(Groups, oppOrphans, 2);

            //Соберем шум
             //var vehicleHashMap = orphansIds.ToDictionary(p => p, p => vehicleHashMap[p]);
             //vehicleHashMap = vehicleHashMap.entrySet().stream().filter(e -> orphansIds.contains(e.getKey())).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
             foreach (var id in orphansIds)
             {
                 Group group = new Group();
                 group.AddVehicle(MyStrategy.AllUnitsDict[id]);
                 Groups.Add(group);
             }
             orphansIds.Clear();


            MyStrategy.Universe.Print($"DbScan: {timer.ElapsedMilliseconds} ms. Groups found {Groups.Count}. Found [{orphansIds.Count}] orphans.");
            return Groups;
        }

        private void Divide(List<Group> Groups, List<Vehicle> sourceVehicles, int minPts)
        {
            if (!sourceVehicles.Any())
                return;

            foreach (VehicleType type in Enum.GetValues(typeof(VehicleType)))
            {
                var vehicles = sourceVehicles.Where(v => v.Type.Equals(type)).ToList();
                divide(Groups, minPts, vehicles);
            }
        }

        private void divide(List<Group> Groups, int minPts, List<Vehicle> vehicles)
        {
            foreach (Vehicle vehicle in vehicles)
            {
                if (visited.Contains(vehicle.Id))
                {
                    continue;
                }
                List<Vehicle> neighbours = regionQuery(vehicles, vehicle);
                if (neighbours.Count < minPts)
                {
                    visited.Add(vehicle.Id);
                    orphansIds.Add(vehicle.Id);
                }
                else
                {
                    var group = new Group();
                    Groups.Add(group);
                    ExpandClaster(vehicle, neighbours, group, vehicles, minPts);
                }
            }
        }

        private void ExpandClaster(Vehicle unit, List<Vehicle> neighbours, Group group, List<Vehicle> vehicles, int minPtsClaster)
        {
            visited.Add(unit.Id);
            group.AddVehicle(unit);
            LinkedList<Vehicle> list = new LinkedList<Vehicle>();
            neighbours.ForEach(v => list.AddLast(v));
            //list.AddAll(neighbours);
            while (list.Any())
            {
                Vehicle vehicle = list.First();
                list.RemoveFirst();

                if (!visited.Contains(vehicle.Id))
                {
                    List<Vehicle> qNeighbours = regionQuery(vehicles, vehicle);
                    if (qNeighbours.Count > minPtsClaster)
                    {
                        qNeighbours.ForEach(v => list.AddLast(v));
                        //list.AddAll(qNeighbours);
                    }
                }
                if (!visited.Contains(vehicle.Id) || orphansIds.Contains(vehicle.Id))
                {
                    group.AddVehicle(vehicle);
                    orphansIds.Remove(vehicle.Id);
                }
                visited.Add(vehicle.Id);
            }
        }

        private List<Vehicle> regionQuery(List<Vehicle> vehicles, Vehicle currentUnit)
        {
            List<Vehicle> neighbours = new List<Vehicle>();
            double pow2Eps = eps * eps;
            foreach (Vehicle vehicle in vehicles)
            {
                if ((currentUnit.X - vehicle.X) * (currentUnit.X - vehicle.X)
                        + (currentUnit.Y - vehicle.Y) * (currentUnit.Y - vehicle.Y) < pow2Eps)
                {
                    neighbours.Add(vehicle);
                }
            }
            return neighbours;
        }
    }

    public class Group
    {
        public List<Vehicle> Vehicles = new List<Vehicle>();
        internal void AddVehicle(Vehicle vehicle)
        {
            Vehicles.Add(vehicle);
        }
    }
}
