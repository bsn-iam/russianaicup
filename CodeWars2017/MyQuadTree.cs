﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.CodeGame.CodeWars2017.DevKit.CSharpCgdk
{
/*
    public class QuadTree<T>
    {
        public static double DEFAULT_EPSILON = 1.0E-6D;

        private Node<T> root = new Node<T>();

        private object xExtractorValue;

        private double xExtractor
        {
            get { return (double) xExtractorValue; }
            set { xExtractorValue = value; }
        }

        private ToDoubleFunction<T> yExtractor;

        private double left;
        private double top;
        private double right;
        private double bottom;

        private double epsilon;

        public QuadTree(
            ToDoubleFunction<T> xExtractor, ToDoubleFunction<T> yExtractor,
            double left, double top, double right, double bottom, double epsilon
        )
        {
            this.xExtractor = xExtractor;
            this.yExtractor = yExtractor;

            this.left = left;
            this.top = top;
            this.right = right;
            this.bottom = bottom;

            this.epsilon = epsilon;
        }

        public QuadTree(
            ToDoubleFunction<T> xExtractor, ToDoubleFunction<T> yExtractor,
            double left, double top, double right, double bottom
        )
        {
            this(xExtractor, yExtractor, left, top, right, bottom, DEFAULT_EPSILON);
        }

        public void Add(T value)
        {
            double x = xExtractor.applyAsDouble(value);
            double y = yExtractor.applyAsDouble(value);

            if (x < left || y < top || x > right || y > bottom)
            {
                throw new ArgumentException(String.Format(
                    "The point (%s, %s) is outside of bounding box (%s, %s, %s, %s).",
                    x, y, left, top, right, bottom
                ));
            }

            Add(value, x, y, root, left, top, right, bottom);
        }

        public void addAll(T[] values)
        {
            foreach (T value in values)
            {
                Add(value);
            }
        }

        public void addAll(IEnumerable<T> values)
        {
            foreach (T value in values)
            {
                Add(value);
            }
        }


        private void Add(
            T value, double x, double y, Node<T> node,
            double left, double top, double right, double bottom
        )
        {
            T currentValue = node.value;

            if (currentValue == null)
            {
                if (node.hasValueBelow)
                {
                    addAsChild(value, x, y, node, left, top, right, bottom);
                }
                else
                {
                    node.value = value;
                }
            }
            else
            {
                double currentX = xExtractor.applyAsDouble(currentValue);
                double currentY = yExtractor.applyAsDouble(currentValue);

                if (Math.Abs(x - currentX) < Double.Epsilon && Math.Abs(y - currentY) < Double.Epsilon)
                {
                    return;
                }

                node.value = null;
                node.hasValueBelow = true;
                node.InitializeChildren();

                addAsChild(currentValue, currentX, currentY, node, left, top, right, bottom);
                addAsChild(value, x, y, node, left, top, right, bottom);
            }
        }

        private void addAsChild(
            T value, double x, double y, Node<T> node,
            double left, double top, double right, double bottom
        )
        {
            double centerX = (left + right) / 2.0D;
            double centerY = (top + bottom) / 2.0D;

            if (x < centerX)
            {
                if (y < centerY)
                {
                    Add(value, x, y, node.leftTop, left, top, centerX, centerY);
                }
                else
                {
                    Add(value, x, y, node.leftBottom, left, centerY, centerX, bottom);
                }
            }
            else
            {
                if (y < centerY)
                {
                    Add(value, x, y, node.rightTop, centerX, top, right, centerY);
                }
                else
                {
                    Add(value, x, y, node.rightBottom, centerX, centerY, right, bottom);
                }
            }
        }

        public T findNearest(T value)
        {
            return findNearest(xExtractor.applyAsDouble(value), yExtractor.applyAsDouble(value));
        }

        public T findNearest(double x, double y)
        {
            return findNearest(x, y, root, left, top, right, bottom);
        }

        public T findNearest(T value, Predicate<T> matcher)
        {
            return findNearest(xExtractor.applyAsDouble(value), yExtractor.applyAsDouble(value), matcher);
        }

        public T findNearest(double x, double y, Predicate<T> matcher)
        {
            return findNearest(x, y, root, left, top, right, bottom, matcher);
        }

        // Equal to call of findNearest(..., Predicate<T> matcher) with (value -> true), but copied for performance reason
        private T findNearest(
            double x, double y, Node<T> node, double left, double top, double right, double bottom
        )
        {
            if (node.value != null)
            {
                return node.value;
            }

            if (!node.hasValueBelow)
            {
                return null;
            }

            double centerX = (left + right) / 2.0D;
            double centerY = (top + bottom) / 2.0D;

            if (x < centerX)
            {
                if (y < centerY)
                {
                    T nearestValue = findNearest(x, y, node.leftTop, left, top, centerX, centerY);
                    double nearestSquaredDistance = getSquaredDistanceTo(nearestValue, x, y);

                    if (nearestSquaredDistance + epsilon >= Math.Sqrt(centerX - x))
                    {
                        T otherValue = findNearest(x, y, node.rightTop, centerX, top, right, centerY);
                        double otherSquaredDistance = getSquaredDistanceTo(otherValue, x, y);

                        if (otherSquaredDistance < nearestSquaredDistance)
                        {
                            nearestValue = otherValue;
                            nearestSquaredDistance = otherSquaredDistance;
                        }
                    }

                    if (nearestSquaredDistance + epsilon >= Math.Sqrt(centerY - y))
                    {
                        T otherValue = findNearest(x, y, node.leftBottom, left, centerY, centerX, bottom);
                        double otherSquaredDistance = getSquaredDistanceTo(otherValue, x, y);

                        if (otherSquaredDistance < nearestSquaredDistance)
                        {
                            nearestValue = otherValue;
                            nearestSquaredDistance = otherSquaredDistance;
                        }
                    }

                    if (nearestSquaredDistance + epsilon >= sumSqr(centerX - x, centerY - y))
                    {
                        T otherValue = findNearest(x, y, node.rightBottom, centerX, centerY, right, bottom);
                        double otherSquaredDistance = getSquaredDistanceTo(otherValue, x, y);

                        if (otherSquaredDistance < nearestSquaredDistance)
                        {
                            nearestValue = otherValue;
                        }
                    }

                    return nearestValue;
                }
                else
                {
                    T nearestValue = findNearest(x, y, node.leftBottom, left, centerY, centerX, bottom);
                    double nearestSquaredDistance = getSquaredDistanceTo(nearestValue, x, y);

                    if (nearestSquaredDistance + epsilon >= Math.Sqrt(centerX - x))
                    {
                        T otherValue = findNearest(x, y, node.rightBottom, centerX, centerY, right, bottom);
                        double otherSquaredDistance = getSquaredDistanceTo(otherValue, x, y);

                        if (otherSquaredDistance < nearestSquaredDistance)
                        {
                            nearestValue = otherValue;
                            nearestSquaredDistance = otherSquaredDistance;
                        }
                    }

                    if (nearestSquaredDistance + epsilon > Math.Sqrt(y - centerY))
                    {
                        T otherValue = findNearest(x, y, node.leftTop, left, top, centerX, centerY);
                        double otherSquaredDistance = getSquaredDistanceTo(otherValue, x, y);

                        if (otherSquaredDistance < nearestSquaredDistance)
                        {
                            nearestValue = otherValue;
                            nearestSquaredDistance = otherSquaredDistance;
                        }
                    }

                    if (nearestSquaredDistance + epsilon >= sumSqr(centerX - x, y - centerY))
                    {
                        T otherValue = findNearest(x, y, node.rightTop, centerX, top, right, centerY);
                        double otherSquaredDistance = getSquaredDistanceTo(otherValue, x, y);

                        if (otherSquaredDistance < nearestSquaredDistance)
                        {
                            nearestValue = otherValue;
                        }
                    }

                    return nearestValue;
                }
            }
            else
            {
                if (y < centerY)
                {
                    T nearestValue = findNearest(x, y, node.rightTop, centerX, top, right, centerY);
                    double nearestSquaredDistance = getSquaredDistanceTo(nearestValue, x, y);

                    if (nearestSquaredDistance + epsilon > Math.Sqrt(x - centerX))
                    {
                        T otherValue = findNearest(x, y, node.leftTop, left, top, centerX, centerY);
                        double otherSquaredDistance = getSquaredDistanceTo(otherValue, x, y);

                        if (otherSquaredDistance < nearestSquaredDistance)
                        {
                            nearestValue = otherValue;
                            nearestSquaredDistance = otherSquaredDistance;
                        }
                    }

                    if (nearestSquaredDistance + epsilon >= Math.Sqrt(centerY - y))
                    {
                        T otherValue = findNearest(x, y, node.rightBottom, centerX, centerY, right, bottom);
                        double otherSquaredDistance = getSquaredDistanceTo(otherValue, x, y);

                        if (otherSquaredDistance < nearestSquaredDistance)
                        {
                            nearestValue = otherValue;
                            nearestSquaredDistance = otherSquaredDistance;
                        }
                    }

                    if (nearestSquaredDistance + epsilon >= sumSqr(x - centerX, centerY - y))
                    {
                        T otherValue = findNearest(x, y, node.leftBottom, left, centerY, centerX, bottom);
                        double otherSquaredDistance = getSquaredDistanceTo(otherValue, x, y);

                        if (otherSquaredDistance < nearestSquaredDistance)
                        {
                            nearestValue = otherValue;
                        }
                    }

                    return nearestValue;
                }
                else
                {
                    T nearestValue = findNearest(x, y, node.rightBottom, centerX, centerY, right, bottom);
                    double nearestSquaredDistance = getSquaredDistanceTo(nearestValue, x, y);

                    if (nearestSquaredDistance + epsilon > Math.Sqrt(x - centerX))
                    {
                        T otherValue = findNearest(x, y, node.leftBottom, left, centerY, centerX, bottom);
                        double otherSquaredDistance = getSquaredDistanceTo(otherValue, x, y);

                        if (otherSquaredDistance < nearestSquaredDistance)
                        {
                            nearestValue = otherValue;
                            nearestSquaredDistance = otherSquaredDistance;
                        }
                    }

                    if (nearestSquaredDistance + epsilon > Math.Sqrt(y - centerY))
                    {
                        T otherValue = findNearest(x, y, node.rightTop, centerX, top, right, centerY);
                        double otherSquaredDistance = getSquaredDistanceTo(otherValue, x, y);

                        if (otherSquaredDistance < nearestSquaredDistance)
                        {
                            nearestValue = otherValue;
                            nearestSquaredDistance = otherSquaredDistance;
                        }
                    }

                    if (nearestSquaredDistance + epsilon > sumSqr(x - centerX, y - centerY))
                    {
                        T otherValue = findNearest(x, y, node.leftTop, left, top, centerX, centerY);
                        double otherSquaredDistance = getSquaredDistanceTo(otherValue, x, y);

                        if (otherSquaredDistance < nearestSquaredDistance)
                        {
                            nearestValue = otherValue;
                        }
                    }

                    return nearestValue;
                }
            }
        }

        //@SuppressWarnings({ "OverlyComplexMethod", "OverlyLongMethod"})
        //@Nullable
        private T findNearest(
            double x, double y, Node<T> node,
            double left, double top, double right, double bottom, Predicate<T> matcher

        )
        {
            if (node.value != null)
            {
                return matcher.Invoke(node.value) ? node.value : null;
            }

            if (!node.hasValueBelow)
            {
                return null;
            }

            double centerX = (left + right) / 2.0D;
            double centerY = (top + bottom) / 2.0D;

            if (x < centerX)
            {
                if (y < centerY)
                {
                    T nearestValue = findNearest(x, y, node.leftTop, left, top, centerX, centerY, matcher);
                    double nearestSquaredDistance = getSquaredDistanceTo(nearestValue, x, y, matcher);

                    if (nearestSquaredDistance + epsilon >= Math.Sqrt(centerX - x))
                    {
                        T otherValue = findNearest(x, y, node.rightTop, centerX, top, right, centerY, matcher);
                        double otherSquaredDistance = getSquaredDistanceTo(otherValue, x, y, matcher);

                        if (otherSquaredDistance < nearestSquaredDistance)
                        {
                            nearestValue = otherValue;
                            nearestSquaredDistance = otherSquaredDistance;
                        }
                    }

                    if (nearestSquaredDistance + epsilon >= Math.Sqrt(centerY - y))
                    {
                        T otherValue = findNearest(x, y, node.leftBottom, left, centerY, centerX, bottom, matcher);
                        double otherSquaredDistance = getSquaredDistanceTo(otherValue, x, y, matcher);

                        if (otherSquaredDistance < nearestSquaredDistance)
                        {
                            nearestValue = otherValue;
                            nearestSquaredDistance = otherSquaredDistance;
                        }
                    }

                    if (nearestSquaredDistance + epsilon >= sumSqr(centerX - x, centerY - y))
                    {
                        T otherValue = findNearest(x, y, node.rightBottom, centerX, centerY, right, bottom, matcher);
                        double otherSquaredDistance = getSquaredDistanceTo(otherValue, x, y, matcher);

                        if (otherSquaredDistance < nearestSquaredDistance)
                        {
                            nearestValue = otherValue;
                        }
                    }

                    return nearestValue;
                }
                else
                {
                    T nearestValue = findNearest(x, y, node.leftBottom, left, centerY, centerX, bottom, matcher);
                    double nearestSquaredDistance = getSquaredDistanceTo(nearestValue, x, y, matcher);

                    if (nearestSquaredDistance + epsilon >= Math.Sqrt(centerX - x))
                    {
                        T otherValue = findNearest(x, y, node.rightBottom, centerX, centerY, right, bottom, matcher);
                        double otherSquaredDistance = getSquaredDistanceTo(otherValue, x, y, matcher);

                        if (otherSquaredDistance < nearestSquaredDistance)
                        {
                            nearestValue = otherValue;
                            nearestSquaredDistance = otherSquaredDistance;
                        }
                    }

                    if (nearestSquaredDistance + epsilon > Math.Sqrt(y - centerY))
                    {
                        T otherValue = findNearest(x, y, node.leftTop, left, top, centerX, centerY, matcher);
                        double otherSquaredDistance = getSquaredDistanceTo(otherValue, x, y, matcher);

                        if (otherSquaredDistance < nearestSquaredDistance)
                        {
                            nearestValue = otherValue;
                            nearestSquaredDistance = otherSquaredDistance;
                        }
                    }

                    if (nearestSquaredDistance + epsilon >= sumSqr(centerX - x, y - centerY))
                    {
                        T otherValue = findNearest(x, y, node.rightTop, centerX, top, right, centerY, matcher);
                        double otherSquaredDistance = getSquaredDistanceTo(otherValue, x, y, matcher);

                        if (otherSquaredDistance < nearestSquaredDistance)
                        {
                            nearestValue = otherValue;
                        }
                    }

                    return nearestValue;
                }
            }
            else
            {
                if (y < centerY)
                {
                    T nearestValue = findNearest(x, y, node.rightTop, centerX, top, right, centerY, matcher);
                    double nearestSquaredDistance = getSquaredDistanceTo(nearestValue, x, y, matcher);

                    if (nearestSquaredDistance + epsilon > Math.Sqrt(x - centerX))
                    {
                        T otherValue = findNearest(x, y, node.leftTop, left, top, centerX, centerY, matcher);
                        double otherSquaredDistance = getSquaredDistanceTo(otherValue, x, y, matcher);

                        if (otherSquaredDistance < nearestSquaredDistance)
                        {
                            nearestValue = otherValue;
                            nearestSquaredDistance = otherSquaredDistance;
                        }
                    }

                    if (nearestSquaredDistance + epsilon >= Math.Sqrt(centerY - y))
                    {
                        T otherValue = findNearest(x, y, node.rightBottom, centerX, centerY, right, bottom, matcher);
                        double otherSquaredDistance = getSquaredDistanceTo(otherValue, x, y, matcher);

                        if (otherSquaredDistance < nearestSquaredDistance)
                        {
                            nearestValue = otherValue;
                            nearestSquaredDistance = otherSquaredDistance;
                        }
                    }

                    if (nearestSquaredDistance + epsilon >= sumSqr(x - centerX, centerY - y))
                    {
                        T otherValue = findNearest(x, y, node.leftBottom, left, centerY, centerX, bottom, matcher);
                        double otherSquaredDistance = getSquaredDistanceTo(otherValue, x, y, matcher);

                        if (otherSquaredDistance < nearestSquaredDistance)
                        {
                            nearestValue = otherValue;
                        }
                    }

                    return nearestValue;
                }
                else
                {
                    T nearestValue = findNearest(x, y, node.rightBottom, centerX, centerY, right, bottom, matcher);
                    double nearestSquaredDistance = getSquaredDistanceTo(nearestValue, x, y, matcher);

                    if (nearestSquaredDistance + epsilon > Math.Sqrt(x - centerX))
                    {
                        T otherValue = findNearest(x, y, node.leftBottom, left, centerY, centerX, bottom, matcher);
                        double otherSquaredDistance = getSquaredDistanceTo(otherValue, x, y, matcher);

                        if (otherSquaredDistance < nearestSquaredDistance)
                        {
                            nearestValue = otherValue;
                            nearestSquaredDistance = otherSquaredDistance;
                        }
                    }

                    if (nearestSquaredDistance + epsilon > Math.Sqrt(y - centerY))
                    {
                        T otherValue = findNearest(x, y, node.rightTop, centerX, top, right, centerY, matcher);
                        double otherSquaredDistance = getSquaredDistanceTo(otherValue, x, y, matcher);

                        if (otherSquaredDistance < nearestSquaredDistance)
                        {
                            nearestValue = otherValue;
                            nearestSquaredDistance = otherSquaredDistance;
                        }
                    }

                    if (nearestSquaredDistance + epsilon > sumSqr(x - centerX, y - centerY))
                    {
                        T otherValue = findNearest(x, y, node.leftTop, left, top, centerX, centerY, matcher);
                        double otherSquaredDistance = getSquaredDistanceTo(otherValue, x, y, matcher);

                        if (otherSquaredDistance < nearestSquaredDistance)
                        {
                            nearestValue = otherValue;
                        }
                    }

                    return nearestValue;
                }
            }
        }

        //@Nonnull
        public List<T> findAllNearby(T value, double squaredDistance)
        {
            return findAllNearby(xExtractor.applyAsDouble(value), yExtractor.applyAsDouble(value), squaredDistance);
        }

        //@Nonnull
        public List<T> findAllNearby(double x, double y, double squaredDistance)
        {
            List<T> values = new List<T>();
            findAllNearby(x, y, squaredDistance, values, root, left, top, right, bottom);
            return values;
        }

        //@Nonnull
        public List<T> findAllNearby(T value, double squaredDistance, Predicate<T> matcher)
        {
            return findAllNearby(xExtractor.applyAsDouble(value), yExtractor.applyAsDouble(value), squaredDistance,
                matcher);
        }

        //@Nonnull
        public List<T> findAllNearby(double x, double y, double squaredDistance, Predicate<T> matcher)
        {
            List<T> values = new List<T>();
            findAllNearby(x, y, squaredDistance, values, root, left, top, right, bottom, matcher);
            return values;
        }

        // Equal to call of findAllNearby(..., Predicate<T> matcher) with (value -> true), but copied for performance reason
        //@SuppressWarnings({ "OverlyComplexMethod", "OverlyLongMethod"})
        private void findAllNearby(
            double x, double y, double squaredDistance, List<T> values, Node<T> node,
            double left, double top, double right, double bottom
        )
        {
            if (node.value != null)
            {
                if (getSquaredDistanceTo(node.value, x, y) <= squaredDistance)
                {
                    values.Add(node.value);
                }
                return;
            }

            if (!node.hasValueBelow)
            {
                return;
            }

            double centerX = (left + right) / 2.0D;
            double centerY = (top + bottom) / 2.0D;

            if (x < centerX)
            {
                if (y < centerY)
                {
                    findAllNearby(x, y, squaredDistance, values, node.leftTop, left, top, centerX, centerY);

                    if (squaredDistance + epsilon >= Math.Sqrt(centerX - x))
                    {
                        findAllNearby(x, y, squaredDistance, values, node.rightTop, centerX, top, right, centerY);
                    }

                    if (squaredDistance + epsilon >= Math.Sqrt(centerY - y))
                    {
                        findAllNearby(x, y, squaredDistance, values, node.leftBottom, left, centerY, centerX, bottom);
                    }

                    if (squaredDistance + epsilon >= sumSqr(centerX - x, centerY - y))
                    {
                        findAllNearby(x, y, squaredDistance, values, node.rightBottom, centerX, centerY, right, bottom);
                    }
                }
                else
                {
                    findAllNearby(x, y, squaredDistance, values, node.leftBottom, left, centerY, centerX, bottom);

                    if (squaredDistance + epsilon >= Math.Sqrt(centerX - x))
                    {
                        findAllNearby(x, y, squaredDistance, values, node.rightBottom, centerX, centerY, right, bottom);
                    }

                    if (squaredDistance + epsilon > Math.Sqrt(y - centerY))
                    {
                        findAllNearby(x, y, squaredDistance, values, node.leftTop, left, top, centerX, centerY);
                    }

                    if (squaredDistance + epsilon >= sumSqr(centerX - x, y - centerY))
                    {
                        findAllNearby(x, y, squaredDistance, values, node.rightTop, centerX, top, right, centerY);
                    }
                }
            }
            else
            {
                if (y < centerY)
                {
                    findAllNearby(x, y, squaredDistance, values, node.rightTop, centerX, top, right, centerY);

                    if (squaredDistance + epsilon > Math.Sqrt(x - centerX))
                    {
                        findAllNearby(x, y, squaredDistance, values, node.leftTop, left, top, centerX, centerY);
                    }

                    if (squaredDistance + epsilon >= Math.Sqrt(centerY - y))
                    {
                        findAllNearby(x, y, squaredDistance, values, node.rightBottom, centerX, centerY, right, bottom);
                    }

                    if (squaredDistance + epsilon >= sumSqr(x - centerX, centerY - y))
                    {
                        findAllNearby(x, y, squaredDistance, values, node.leftBottom, left, centerY, centerX, bottom);
                    }
                }
                else
                {
                    findAllNearby(x, y, squaredDistance, values, node.rightBottom, centerX, centerY, right, bottom);

                    if (squaredDistance + epsilon > Math.Sqrt(x - centerX))
                    {
                        findAllNearby(x, y, squaredDistance, values, node.leftBottom, left, centerY, centerX, bottom);
                    }

                    if (squaredDistance + epsilon > Math.Sqrt(y - centerY))
                    {
                        findAllNearby(x, y, squaredDistance, values, node.rightTop, centerX, top, right, centerY);
                    }

                    if (squaredDistance + epsilon > sumSqr(x - centerX, y - centerY))
                    {
                        findAllNearby(x, y, squaredDistance, values, node.leftTop, left, top, centerX, centerY);
                    }
                }
            }
        }

        //@SuppressWarnings({ "OverlyComplexMethod", "OverlyLongMethod"})
        private void findAllNearby(
            double x, double y, double squaredDistance, List<T> values, Node<T> node,
            double left, double top, double right, double bottom, Predicate<T> matcher
        )
        {
            if (node.value != null)
            {
                if (getSquaredDistanceTo(node.value, x, y, matcher) <= squaredDistance)
                {
                    values.Add(node.value);
                }
                return;
            }

            if (!node.hasValueBelow)
            {
                return;
            }

            double centerX = (left + right) / 2.0D;
            double centerY = (top + bottom) / 2.0D;

            if (x < centerX)
            {
                if (y < centerY)
                {
                    findAllNearby(x, y, squaredDistance, values, node.leftTop, left, top, centerX, centerY, matcher);

                    if (squaredDistance + epsilon >= Math.Sqrt(centerX - x))
                    {
                        findAllNearby(x, y, squaredDistance, values, node.rightTop, centerX, top, right, centerY,
                            matcher);
                    }

                    if (squaredDistance + epsilon >= Math.Sqrt(centerY - y))
                    {
                        findAllNearby(x, y, squaredDistance, values, node.leftBottom, left, centerY, centerX, bottom,
                            matcher);
                    }

                    if (squaredDistance + epsilon >= sumSqr(centerX - x, centerY - y))
                    {
                        findAllNearby(x, y, squaredDistance, values, node.rightBottom, centerX, centerY, right, bottom,
                            matcher);
                    }
                }
                else
                {
                    findAllNearby(x, y, squaredDistance, values, node.leftBottom, left, centerY, centerX, bottom,
                        matcher);
                    if (squaredDistance + epsilon >= Math.Sqrt(centerX - x))
                    {
                        findAllNearby(x, y, squaredDistance, values, node.rightBottom, centerX, centerY, right, bottom,
                            matcher);
                    }

                    if (squaredDistance + epsilon > Math.Sqrt(y - centerY))
                    {
                        findAllNearby(x, y, squaredDistance, values, node.leftTop, left, top, centerX, centerY,
                            matcher);
                    }

                    if (squaredDistance + epsilon >= sumSqr(centerX - x, y - centerY))
                    {
                        findAllNearby(x, y, squaredDistance, values, node.rightTop, centerX, top, right, centerY,
                            matcher);
                    }
                }
            }
            else
            {
                if (y < centerY)
                {
                    findAllNearby(x, y, squaredDistance, values, node.rightTop, centerX, top, right, centerY, matcher);

                    if (squaredDistance + epsilon > Math.Sqrt(x - centerX))
                    {
                        findAllNearby(x, y, squaredDistance, values, node.leftTop, left, top, centerX, centerY,
                            matcher);
                    }

                    if (squaredDistance + epsilon >= Math.Sqrt(centerY - y))
                    {
                        findAllNearby(x, y, squaredDistance, values, node.rightBottom, centerX, centerY, right, bottom,
                            matcher);
                    }

                    if (squaredDistance + epsilon >= sumSqr(x - centerX, centerY - y))
                    {
                        findAllNearby(x, y, squaredDistance, values, node.leftBottom, left, centerY, centerX, bottom,
                            matcher);
                    }
                }
                else
                {
                    findAllNearby(x, y, squaredDistance, values, node.rightBottom, centerX, centerY, right, bottom,
                        matcher);

                    if (squaredDistance + epsilon > Math.Sqrt(x - centerX))
                    {
                        findAllNearby(x, y, squaredDistance, values, node.leftBottom, left, centerY, centerX, bottom,
                            matcher);
                    }

                    if (squaredDistance + epsilon > Math.Sqrt(y - centerY))
                    {
                        findAllNearby(x, y, squaredDistance, values, node.rightTop, centerX, top, right, centerY,
                            matcher);
                    }

                    if (squaredDistance + epsilon > sumSqr(x - centerX, y - centerY))
                    {
                        findAllNearby(x, y, squaredDistance, values, node.leftTop, left, top, centerX, centerY,
                            matcher);
                    }
                }
            }
        }

        public bool hasNearby(T value, double squaredDistance)
        {
            return hasNearby(xExtractor.applyAsDouble(value), yExtractor.applyAsDouble(value), squaredDistance);
        }

        public bool hasNearby(double x, double y, double squaredDistance)
        {
            return hasNearby(x, y, squaredDistance, root, left, top, right, bottom);
        }

        public bool hasNearby(T value, double squaredDistance, Predicate<T> matcher)
        {
            return hasNearby(xExtractor.applyAsDouble(value), yExtractor.applyAsDouble(value), squaredDistance,
                matcher);
        }

        public bool hasNearby(double x, double y, double squaredDistance, Predicate<T> matcher)
        {
            return hasNearby(x, y, squaredDistance, root, left, top, right, bottom, matcher);
        }

        // Equal to call of hasNearby(..., Predicate<T> matcher) with (value -> true), but copied for performance reason
        //@SuppressWarnings({ "OverlyComplexMethod", "OverlyLongMethod"})
        private bool hasNearby(
            double x, double y, double squaredDistance, Node<T> node,
            double left, double top, double right, double bottom
        )
        {
            if (node.value != null)
            {
                return getSquaredDistanceTo(node.value, x, y) <= squaredDistance;
            }

            if (!node.hasValueBelow)
            {
                return false;
            }

            double centerX = (left + right) / 2.0D;
            double centerY = (top + bottom) / 2.0D;

            if (x < centerX)
            {
                if (y < centerY)
                {
                    if (hasNearby(x, y, squaredDistance, node.leftTop, left, top, centerX, centerY))
                    {
                        return true;
                    }

                    if (squaredDistance + epsilon >= Math.Sqrt(centerX - x))
                    {
                        if (hasNearby(x, y, squaredDistance, node.rightTop, centerX, top, right, centerY))
                        {
                            return true;
                        }
                    }

                    if (squaredDistance + epsilon >= Math.Sqrt(centerY - y))
                    {
                        if (hasNearby(x, y, squaredDistance, node.leftBottom, left, centerY, centerX, bottom))
                        {
                            return true;
                        }
                    }

                    if (squaredDistance + epsilon >= sumSqr(centerX - x, centerY - y))
                    {
                        if (hasNearby(x, y, squaredDistance, node.rightBottom, centerX, centerY, right, bottom))
                        {
                            return true;
                        }
                    }

                    return false;
                }
                else
                {
                    if (hasNearby(x, y, squaredDistance, node.leftBottom, left, centerY, centerX, bottom))
                    {
                        return true;
                    }

                    if (squaredDistance + epsilon >= Math.Sqrt(centerX - x))
                    {
                        if (hasNearby(x, y, squaredDistance, node.rightBottom, centerX, centerY, right, bottom))
                        {
                            return true;
                        }
                    }

                    if (squaredDistance + epsilon > Math.Sqrt(y - centerY))
                    {
                        if (hasNearby(x, y, squaredDistance, node.leftTop, left, top, centerX, centerY))
                        {
                            return true;
                        }
                    }

                    if (squaredDistance + epsilon >= sumSqr(centerX - x, y - centerY))
                    {
                        if (hasNearby(x, y, squaredDistance, node.rightTop, centerX, top, right, centerY))
                        {
                            return true;
                        }
                    }

                    return false;
                }
            }
            else
            {
                if (y < centerY)
                {
                    if (hasNearby(x, y, squaredDistance, node.rightTop, centerX, top, right, centerY))
                    {
                        return true;
                    }

                    if (squaredDistance + epsilon > Math.Sqrt(x - centerX))
                    {
                        if (hasNearby(x, y, squaredDistance, node.leftTop, left, top, centerX, centerY))
                        {
                            return true;
                        }
                    }

                    if (squaredDistance + epsilon >= Math.Sqrt(centerY - y))
                    {
                        if (hasNearby(x, y, squaredDistance, node.rightBottom, centerX, centerY, right, bottom))
                        {
                            return true;
                        }
                    }

                    if (squaredDistance + epsilon >= sumSqr(x - centerX, centerY - y))
                    {
                        if (hasNearby(x, y, squaredDistance, node.leftBottom, left, centerY, centerX, bottom))
                        {
                            return true;
                        }
                    }

                    return false;
                }
                else
                {
                    if (hasNearby(x, y, squaredDistance, node.rightBottom, centerX, centerY, right, bottom))
                    {
                        return true;
                    }

                    if (squaredDistance + epsilon > Math.Sqrt(x - centerX))
                    {
                        if (hasNearby(x, y, squaredDistance, node.leftBottom, left, centerY, centerX, bottom))
                        {
                            return true;
                        }
                    }

                    if (squaredDistance + epsilon > Math.Sqrt(y - centerY))
                    {
                        if (hasNearby(x, y, squaredDistance, node.rightTop, centerX, top, right, centerY))
                        {
                            return true;
                        }
                    }

                    if (squaredDistance + epsilon > sumSqr(x - centerX, y - centerY))
                    {
                        if (hasNearby(x, y, squaredDistance, node.leftTop, left, top, centerX, centerY))
                        {
                            return true;
                        }
                    }

                    return false;
                }
            }
        }

        //@SuppressWarnings({ "OverlyComplexMethod", "OverlyLongMethod"})
        private bool hasNearby(
            double x, double y, double squaredDistance, Node<T> node,
            double left, double top, double right, double bottom, Predicate<T> matcher
        )
        {
            if (node.value != null)
            {
                return getSquaredDistanceTo(node.value, x, y, matcher) <= squaredDistance;
            }

            if (!node.hasValueBelow)
            {
                return false;
            }

            double centerX = (left + right) / 2.0D;
            double centerY = (top + bottom) / 2.0D;

            if (x < centerX)
            {
                if (y < centerY)
                {
                    if (hasNearby(x, y, squaredDistance, node.leftTop, left, top, centerX, centerY, matcher))
                    {
                        return true;
                    }

                    if (squaredDistance + epsilon >= Math.Sqrt(centerX - x))
                    {
                        if (hasNearby(x, y, squaredDistance, node.rightTop, centerX, top, right, centerY, matcher))
                        {
                            return true;
                        }
                    }

                    if (squaredDistance + epsilon >= Math.Sqrt(centerY - y))
                    {
                        if (hasNearby(x, y, squaredDistance, node.leftBottom, left, centerY, centerX, bottom, matcher))
                        {
                            return true;
                        }
                    }

                    if (squaredDistance + epsilon >= sumSqr(centerX - x, centerY - y))
                    {
                        if (hasNearby(x, y, squaredDistance, node.rightBottom, centerX, centerY, right, bottom,
                            matcher))
                        {
                            return true;
                        }
                    }

                    return false;
                }
                else
                {
                    if (hasNearby(x, y, squaredDistance, node.leftBottom, left, centerY, centerX, bottom, matcher))
                    {
                        return true;
                    }

                    if (squaredDistance + epsilon >= Math.Sqrt(centerX - x))
                    {
                        if (hasNearby(x, y, squaredDistance, node.rightBottom, centerX, centerY, right, bottom,
                            matcher))
                        {
                            return true;
                        }
                    }

                    if (squaredDistance + epsilon > Math.Sqrt(y - centerY))
                    {
                        if (hasNearby(x, y, squaredDistance, node.leftTop, left, top, centerX, centerY, matcher))
                        {
                            return true;
                        }
                    }

                    if (squaredDistance + epsilon >= sumSqr(centerX - x, y - centerY))
                    {
                        if (hasNearby(x, y, squaredDistance, node.rightTop, centerX, top, right, centerY, matcher))
                        {
                            return true;
                        }
                    }

                    return false;
                }
            }
            else
            {
                if (y < centerY)
                {
                    if (hasNearby(x, y, squaredDistance, node.rightTop, centerX, top, right, centerY, matcher))
                    {
                        return true;
                    }

                    if (squaredDistance + epsilon > Math.Sqrt(x - centerX))
                    {
                        if (hasNearby(x, y, squaredDistance, node.leftTop, left, top, centerX, centerY, matcher))
                        {
                            return true;
                        }
                    }

                    if (squaredDistance + epsilon >= Math.Sqrt(centerY - y))
                    {
                        if (hasNearby(x, y, squaredDistance, node.rightBottom, centerX, centerY, right, bottom,
                            matcher))
                        {
                            return true;
                        }
                    }

                    if (squaredDistance + epsilon >= sumSqr(x - centerX, centerY - y))
                    {
                        if (hasNearby(x, y, squaredDistance, node.leftBottom, left, centerY, centerX, bottom, matcher))
                        {
                            return true;
                        }
                    }

                    return false;
                }
                else
                {
                    if (hasNearby(x, y, squaredDistance, node.rightBottom, centerX, centerY, right, bottom, matcher))
                    {
                        return true;
                    }

                    if (squaredDistance + epsilon > Math.Sqrt(x - centerX))
                    {
                        if (hasNearby(x, y, squaredDistance, node.leftBottom, left, centerY, centerX, bottom, matcher))
                        {
                            return true;
                        }
                    }

                    if (squaredDistance + epsilon > Math.Sqrt(y - centerY))
                    {
                        if (hasNearby(x, y, squaredDistance, node.rightTop, centerX, top, right, centerY, matcher))
                        {
                            return true;
                        }
                    }

                    if (squaredDistance + epsilon > sumSqr(x - centerX, y - centerY))
                    {
                        if (hasNearby(x, y, squaredDistance, node.leftTop, left, top, centerX, centerY, matcher))
                        {
                            return true;
                        }
                    }

                    return false;
                }
            }
        }

        public void clear()
        {
            clear(root);
        }

        private static void clear(Node<T> node)
        {
            node.value = null;

            if (node.hasValueBelow)
            {
                node.hasValueBelow = false;

                clear(node.leftTop);
                clear(node.rightTop);
                clear(node.leftBottom);
                clear(node.rightBottom);
            }
        }

        private double getSquaredDistanceTo(T value, double x, double y)
        {
            return value == null
                ? Double.PositiveInfinity
                : sumSqr(
                    xExtractor.applyAsDouble(value) - x, yExtractor.applyAsDouble(value) - y
                );
        }

        private double sumSqr(object p1, object p2)
        {
            return Math.Sqrt((double) p1) + Math.Sqrt((double) p2);
        }

        private double getSquaredDistanceTo(T value, double x, double y, Predicate<T> matcher)
        {

            return value == null || !matcher.Invoke(value)
                ? Double.PositiveInfinity
                : sumSqr(
                    xExtractor.applyAsDouble(value) - x, yExtractor.applyAsDouble(value) - y
                );
        }
    }
*/

    internal class Node<T> : INullable
    {
        //@Nullable
        public T value;

        public bool hasValueBelow;

        public Node<T> leftTop;
        public Node<T> rightTop;
        public Node<T> leftBottom;
        public Node<T> rightBottom;

        public bool IsNull
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public void InitializeChildren()
        {
            if (leftTop == null)
            {
                leftTop = new Node<T>();
                rightTop = new Node<T>();
                leftBottom = new Node<T>();
                rightBottom = new Node<T>();
            }
        }
    }
}
