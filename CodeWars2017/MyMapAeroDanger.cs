﻿using System;
using System.Collections.Generic;
using System.Linq;
using Com.CodeGame.CodeWars2017.DevKit.CSharpCgdk.Model;

namespace Com.CodeGame.CodeWars2017.DevKit.CSharpCgdk
{
    internal class MapAeroDanger : BonusMap, IBonusMap
    {
        private readonly List<Vehicle> units;
        private readonly AbsolutePosition squadCenter;

        public MapAeroDanger(List<Vehicle> units, AbsolutePosition squadCenter)
        {
            this.units = units;
            this.squadCenter = squadCenter;
        }

        public void GenerateMap(MapType type, int weigth, double range)
        {
            var enemyUnitsForMap = units.Where(
                u => (u.X - squadCenter.X) < range &&
                     (u.Y - squadCenter.Y) < range
            ).ToList();

            foreach (var unit in enemyUnitsForMap)
                AddUnitCalculation(unit, unit.AerialAttackRange * 1, unit.AerialDamage, unit.AerialAttackRange * 2.5);
        }
        
    }
}