@Echo off
set source=D:\BitBucket\russianaicup\CodeBall2018
set store=D:\BitBucket\CodeBall_versions

::SET /P version="Which version? "
set version=80

pushd %source%

dotnet publish -c Release -r win10-x64
set path=D:\BitBucket\russianaicup\CodeBall2018\codeball2018-windows
set store=D:\BitBucket\CodeBall_versions

echo.
echo Starting with version %version%..

:loop

::taskkill /IM codeball2018.exe /F
start /MIN %path%\codeball2018.exe --p1-name bsn-iam --p2-name version_%version% --no-countdown --p2 tcp-310%version% --p1 tcp-31001 --noshow

start /MIN %store%\codeball_v%version%\Release\Strategy.exe

call %source%\Strategy\bin\Release\netcoreapp2.1\win10-x64\Strategy.exe
echo.
goto :loop

pause
