@echo on

set version=80


taskkill /IM codeball2018.exe /F
del -Y %path%\log.txt
set path=D:\BitBucket\russianaicup\CodeBall2018\codeball2018-windows

set store=D:\BitBucket\CodeBall_versions

start %path%\codeball2018.exe --log-file %path%\log.txt --export-arena %path%\arena.obj --p1-name bsn-iam --p2-name version_%version% --no-countdown --start-paused --p2 tcp-310%version% --nitro true

start /MIN %store%\codeball_v%version%\Release\Strategy.exe

:: --nitro true

::--team-size <size> — Размер команды (по умолчанию 2).
::--p1 <player1> — Первый игрок (по умолчанию tcp-31001). Возможные значения: tcp-<port>, keyboard, helper, empty
::--p2 <player2> — Второй игрок (по умолчанию helper). Возможные значения: tcp-<port>, keyboard, helper, empty
::--p1-name <player_name>/--p2-name <player_name> — Имена игроков для отображения в таблице со счетом
::--nitro <true/false> — Вкл/выкл нитро в игре (по умолчанию false)
::--duration <ticks> — Длительность игры в тиках. по умолчанию 5 * 60 * 60 = 18000.
::--log-file <path> — Путь к файлу для сохранения лога игры
::--replay <path> — Путь к файлу для воспроизведения лога игры
::--noshow — отключить визуализацию
::--start-paused — пауза при старте
::--no-countdown — отключить отсчёт в начале игры и после каждого гола
::--until-first-goal — закончить игру при первом забитом голе
::--fast-forward <tick> — начать просмотр игры с заданного тика
::--seed <int> — задать seed игры
::--vsync — включить вертикальную синхронизацию
::--export-arena <path> — экспорт модели арены в формате obj в заданный файл