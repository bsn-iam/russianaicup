@echo off
set source=D:\BitBucket\russianaicup\CodeBall2018
set store=D:\BitBucket\CodeBall_versions

SET /P version="Which version? "

pushd %source%


dotnet publish -c Debug -r win10-x64
dotnet publish -c Release -r win10-x64

xcopy /Y /S /Q %source%\Strategy\bin\Release\netcoreapp2.1\win10-x64 %store%\codeball_v%version%\Release\*
xcopy /Y /S /Q %source%\Strategy\bin\Debug\netcoreapp2.1\win10-x64 %store%\codeball_v%version%\Debug\*

pause