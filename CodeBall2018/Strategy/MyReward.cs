﻿namespace Com.CodeGame.CodeBall2018.DevKit.CSharpCgdk
{
    public class Reward
    {
        public string Label { get; }
        public double Value { get; }
        public int Weight { get; }

        public Reward(string label, double value, int weight)
        {
            this.Label = label;
            this.Value = value;
            this.Weight = weight;
        }
        public override string ToString()
        {
            return $"{Label} - v[{Value:f4}], w[{Weight:f4}], total[{(Value * Weight):f4}]";
        }
    }
}