﻿using System.Collections.Generic;
using System.Linq;


namespace Com.CodeGame.CodeBall2018.DevKit.CSharpCgdk
{
    public class Branch
    {
        public WorldState InitialState { get; private set; }
        public WorldState FinalState { get; set; }
        public Order Command { get; private set; }
        public List<Point3> Route { get; set; } = new List<Point3>();
        public List<int> ChildIDs { get; private set; } = new List<int>();
        public WorldState ForkState => FinalState;
        public int ParentId { get; private set; } = 0;
        public bool IsRoot { get; } = false;
        public int Id { get; internal set; } = 0;

        public bool IsLineEnd => !ChildIDs.Any();

        public List<Reward> RewardList { get; internal set; } = new List<Reward>();

        public double RewardValue
        {
            get
            {
                double result = 0;
                foreach (var reward in RewardList)
                    result += reward.Value * reward.Weight;
                return result;
            }
        }

        public void AddChild(int childBranchId)
        {
            ChildIDs.Add(childBranchId);
        }

        internal void SetParent(int parentId)
        {
            ParentId = parentId;
        }

        public Branch (WorldState initialState, Order command, int id, bool isRoot = false)
        {
            this.InitialState = initialState;
            this.Command = command;
            this.Id = id;
            this.IsRoot = isRoot;
        }
    }
}
