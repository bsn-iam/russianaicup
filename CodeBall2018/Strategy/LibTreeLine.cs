﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.CodeGame.CodeBall2018.DevKit.CSharpCgdk
{
    public class TreeLine
    {
        public List<int> BranchIDs = new List<int>();
        public double RewardSum { get; private set; } = 0;
        public double TicksSum { get; private set; } = 0;
        private double LineReward = 0;
        private double LineLengthKoeffBase = 1.015;
        public int LegsAmount { get; private set; } = 0;

        public TreeLine()
        {
        }

        public TreeLine(Branch newBranch)
        {
            Extend(newBranch);
        }

        public TreeLine Clone()
        {
            var clone = new TreeLine();
            clone.BranchIDs = new List<int>(BranchIDs);
            clone.RewardSum = RewardSum;
            clone.TicksSum = TicksSum;
            clone.LegsAmount = LegsAmount;
            return clone;
        }

        public void Extend(Branch newBranch)
        {
            BranchIDs.Add(newBranch.Id);
            RewardSum += newBranch.RewardValue;
            TicksSum += newBranch.Route.Count;
            LegsAmount++;
        }

        public double GetLineReward()
        {
            if (LineReward == 0)
            {
                var lengthNegativeKoeff = 1 / Math.Pow(LineLengthKoeffBase, TicksSum);
                LineReward = RewardSum / TicksSum * lengthNegativeKoeff;
            }
            return LineReward;
        }
    }
}
