﻿using System;
using Com.CodeGame.CodeBall2018.DevKit.CSharpCgdk.Model;

namespace Com.CodeGame.CodeBall2018.DevKit.CSharpCgdk
{

    public class MyNitroPack
    {
        public int Id;
        public Point3 Position;
        public double Radius;
        public double NitroAmount;
        public int? RespawnTicks;
        public bool IsAlive;

        public MyNitroPack(NitroPack pack)
        {
            Id = pack.id;
            Position = new Point3(pack.x, pack.y, pack.z);
            Radius = pack.radius;
            NitroAmount = pack.nitro_amount;
            RespawnTicks = pack.respawn_ticks;
            IsAlive = true;
        }

        public MyNitroPack(int id, Point3 position, double radius, double nitroAmount, int? respawnTicks, bool isAlive)
        {
            Id = id;
            Position = position;
            Radius = radius;
            NitroAmount = nitroAmount;
            RespawnTicks = respawnTicks;
            IsAlive = isAlive;
        }

        internal MyNitroPack LazyClone()
        {
            return new MyNitroPack(Id, Position.Clone(), Radius, NitroAmount, RespawnTicks, IsAlive);
        }

        internal void Update(NitroPack pack)
        {
            Position = new Point3(pack.x, pack.y, pack.z);
            Radius = pack.radius;
            NitroAmount = pack.nitro_amount;
            RespawnTicks = pack.respawn_ticks;
        }
    }
}