using Com.CodeGame.CodeBall2018.DevKit.CSharpCgdk.Model;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Diagnostics;

namespace Com.CodeGame.CodeBall2018.DevKit.CSharpCgdk
{
    public sealed class MyStrategy : IStrategy
    {
        public static Constants Constants;
        public Stopwatch MainTimer = new Stopwatch();
        public Stopwatch TotalTimer = new Stopwatch();
        public double DeadLinePosition;
        public Tree Tree;
        public MyBall MyBall;
        public Dictionary<int, MyRobot> MyRobots = new Dictionary<int, MyRobot>();
        public static ActionSelector ActionSelector;
        private string DebugOutput = "";
        private int CurrentRobotId;
        private OutputGenerator OutputGenerator = new OutputGenerator();
        public static Dictionary<int, Approximator> OrderGenerationTimers = new Dictionary<int, Approximator>();
        public Dictionary<int, MyNitroPack> MyNitroPacks = new Dictionary<int, MyNitroPack>();
        private int Score = 0;
        private int Score_prev = 0;
        public static double TotalTimeAverage = 0;
        public int GameStartTick = 0;

        public void Act(Robot me, Rules rules, Game game, Action action)
        {
            MainTimer.Restart();
            TotalTimer.Start();
            if (MyBall == null)
                Init(rules);

            action = GenerateOrder(me, rules, game, action);

            //if (System.Math.Abs(MyBall.Position.X) < double.Epsilon &&
            //    System.Math.Abs(MyBall.Position.Z) < double.Epsilon &&
            //    System.Math.Abs(MyBall.Speed.Y) < double.Epsilon)
            //{
            //    GameStartTick = game.current_tick;
            //    TotalTimer.Reset();
            //}


            foreach (var player in game.players)
                if (player.me && player.strategy_crashed)
                    throw new System.Exception("Crashed.");

            if (!OrderGenerationTimers.ContainsKey(me.id))
                OrderGenerationTimers.Add(me.id, new Approximator(10));
            OrderGenerationTimers[me.id].AddValue(MainTimer.Elapsed.Milliseconds);

            TotalTimer.Stop();
            TotalTimeAverage = (double)TotalTimer.ElapsedMilliseconds / (game.current_tick - GameStartTick + 1);
        }

        public string CustomRendering() => DebugOutput;

        private Action GenerateOrder(Robot me, Rules rules, Game game, Action action)
        {
            UpdateMy(me, game);

            var worldState = new WorldState(CurrentRobotId, MyBall, MyRobots, MyNitroPacks, game.current_tick);
            var order = ActionSelector.Generate(worldState);

#if DEBUG
            DebugOutput = OutputGenerator.GenerateOutput(worldState);
            //Visualizer.Draw(worldState);
#endif

            return TransformToAction(order, action);
        }

        private void UpdateMy(Robot me, Game game)
        {
            MyBall.Update(game.ball);

            CurrentRobotId = me.id;

            foreach (var robot in game.robots)
            {
                var id = robot.id;
                if (!MyRobots.ContainsKey(id))
                    MyRobots.Add(id, new MyRobot(id, robot.is_teammate));
                MyRobots[id].Update(robot);
            }

            foreach (var pack in game.nitro_packs)
            {
                var id = pack.id;
                if (!MyNitroPacks.ContainsKey(id))
                    MyNitroPacks.Add(id, new MyNitroPack(pack));
                MyNitroPacks[id].Update(pack);
            }
        }

        private void Init(Rules rules)
        {
            //AttachDebugger();
            Constants = new Constants(rules);
            ActionSelector = new ActionSelector();
            MyBall = new MyBall();
        }

        private void AttachDebugger()
        {
#if DEBUG
            Debugger.Launch();
#endif
        }

        private Action TransformToAction(Order order, Action action)
        {
            action.target_velocity_x = order.TargetSpeed.X;
            action.target_velocity_y = order.TargetSpeed.Y;
            action.target_velocity_z = order.TargetSpeed.Z;
            action.jump_speed = order.JumpSpeed;
            action.use_nitro = order.UseNitro;

            return action;
        }
    }
}
