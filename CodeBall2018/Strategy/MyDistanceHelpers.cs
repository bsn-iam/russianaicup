﻿using Com.CodeGame.CodeBall2018.DevKit.CSharpCgdk.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Com.CodeGame.CodeBall2018.DevKit.CSharpCgdk
{
    public class DistNorm
    {
        public double Distance;
        public Vector3 Normal;

        public DistNorm(double distance, Vector3 normal)
        {
            Distance = distance;
            Normal = normal;
        }

        public DistNorm GetMinWith(DistNorm dn2)
        {
            if (this.Distance < dn2.Distance)
                return this;
            return dn2;
        }
    }

    public static class MyDistanceHelpers
    {
        internal static bool DoIWantToPush(MyRobot currentRobot, MyBall ball)
        {
            var result = false;
            var pushTicksBeforeCollision = 1.7;
            if (currentRobot.Position.Z < ball.Position.Z &&
                (currentRobot.Position - ball.Position).Length < 5 * ball.Radius)
            {
                var speedPerTickNow = currentRobot.Speed.Length / MyStrategy.Constants.TICKS_PER_SECOND;
                var distanceToPushStart = speedPerTickNow * pushTicksBeforeCollision;
                result = AreInCollisionOrCloser(currentRobot, ball, distanceToPushStart);
            }
            return result;
        }

        public static bool IsGoalScored(WorldState worldState)
        {
            var ball = worldState.MyBall;
            var gameIsOver = Math.Abs(ball.Position.Z) > MyStrategy.Constants.Arena.depth / 2 + ball.Radius;
            return gameIsOver;
        }

        public static bool AreInCollisionOrCloser(ISimObject a, ISimObject b, double distance = 0)
        {
            double penetration = CalculatePenetration(a, b);
            return penetration + distance > 0;
        }

        public static double CalculatePenetration(ISimObject a, ISimObject b)
        {
            var delta_Position = b.Position - a.Position;
            var distance = delta_Position.Length;
            var penetration = a.Radius + b.Radius - distance;
            return penetration;
        }

        public static DistNorm distance_to_plane(Point3 point, Point3 point_on_plane, Vector3 plane_normal)
        {
            return new DistNorm((point - point_on_plane) * plane_normal,
                                plane_normal);
        }

        public static DistNorm distance_to_sphere_inner(Point3 point, Point3 sphere_center, double sphere_radius)
        {
            return new DistNorm(sphere_radius - (point - sphere_center).Length,
                                (sphere_center - point).Normalize().toVector3());
        }

        public static DistNorm distance_to_sphere_outer(Point3 point, Point3 sphere_center, double sphere_radius)
        {
            return new DistNorm((point - sphere_center).Length - sphere_radius,
                                (point - sphere_center).Normalize().toVector3());
        }

        public static DistNorm distance_to_arena_quarter(Point3 point, Arena Arena, double objRadius)
        {
            // Ground
            var distance = 
                        //distance_to_plane(point, new Point3(0, 0, 0), new Vector3(0, 1, 0)));
                        new DistNorm(point.Y, new Vector3(0, 1, 0));

            // Roof
            distance = distance.GetMinWith(
                        //distance_to_plane(point, new Point3(0, Arena.height, 0), new Vector3(0, -1, 0)));
                        new DistNorm(Arena.height - point.Y, new Vector3(0, -1, 0)));

            // Side x
            distance = distance.GetMinWith(
                //distance_to_plane(point, new Point3(Arena.width / 2, 0, 0), new Vector3(-1, 0, 0)));
                new DistNorm(Arena.width / 2 - point.X, new Vector3(-1, 0, 0)));


            // // Side z (goal)
            // distance = distance.GetMinWith(
            //             distance_to_plane(
            //                     point, new Point3(0, 0, (Arena.depth / 2) + Arena.goal_depth), new Vector3(0, 0, -1)));

            // Side z
            var v = new Point(point.X, point.Y) - new Point((Arena.goal_width / 2) - Arena.goal_top_radius, Arena.goal_height - Arena.goal_top_radius);
            if (point.X >= (Arena.goal_width / 2) + Arena.goal_side_radius ||
                point.Y >= Arena.goal_height + Arena.goal_side_radius ||
                v.X > 0 && v.Y > 0 && v.Length >= Arena.goal_top_radius + Arena.goal_side_radius)
            {
                distance = distance.GetMinWith(distance_to_plane(point, new Point3(0, 0, Arena.depth / 2), new Vector3(0, 0, -1)));
            }

            // Side x & ceiling (goal)
            if (point.Z >= (Arena.depth / 2) + Arena.goal_side_radius)
            {
                // x
                distance = distance.GetMinWith(distance_to_plane(point, new Point3(Arena.goal_width / 2, 0, 0), new Vector3(-1, 0, 0)));
                // y
                distance = distance.GetMinWith(distance_to_plane(point, new Point3(0, Arena.goal_height, 0), new Vector3(0, -1, 0)));
            }

            // Goal back corners
            //assert Arena.bottom_radius == Arena.goal_top_radius
            if (point.Z > (Arena.depth / 2) + Arena.goal_depth - Arena.bottom_radius)
            {
                var sphereCenter = new Point3(
                    Math.Clamp(point.X, Arena.bottom_radius - (Arena.goal_width / 2), (Arena.goal_width / 2) - Arena.bottom_radius),
                    Math.Clamp(point.Y, Arena.bottom_radius, Arena.goal_height - Arena.goal_top_radius),
                    (Arena.depth / 2) + Arena.goal_depth - Arena.bottom_radius);

                distance = distance.GetMinWith(
                        distance_to_sphere_inner(point, sphereCenter, Arena.bottom_radius));
            }

            // Corner
            if (point.X > (Arena.width / 2) - Arena.corner_radius &&
                    point.Z > (Arena.depth / 2) - Arena.corner_radius)
            {
                distance = distance.GetMinWith(distance_to_sphere_inner(
                point, new Point3((Arena.width / 2) - Arena.corner_radius, point.Y, (Arena.depth / 2) - Arena.corner_radius),
                Arena.corner_radius));
            }

            // Goal outer corner
            if (point.Z < (Arena.depth / 2) + Arena.goal_side_radius)
            {
                // Side x
                if (point.X < (Arena.goal_width / 2) + Arena.goal_side_radius)
                {
                    distance = distance.GetMinWith(
                            distance_to_sphere_outer(
                                        point,
                                        new Point3((Arena.goal_width / 2) + Arena.goal_side_radius, point.Y, (Arena.depth / 2) + Arena.goal_side_radius),
                                        Arena.goal_side_radius));
                }

                // Ceiling
                if (point.Y < Arena.goal_height + Arena.goal_side_radius)
                {
                    distance = distance.GetMinWith(
                                distance_to_sphere_outer(
                                        point,
                                        new Point3(point.X, Arena.goal_height + Arena.goal_side_radius, (Arena.depth / 2) + Arena.goal_side_radius),
                                        Arena.goal_side_radius));
                }

                // Top corner
                var o1 = new Point((Arena.goal_width / 2) - Arena.goal_top_radius, Arena.goal_height - Arena.goal_top_radius);
                var v1 = new Point(point.X, point.Y) - o1;

                if (v.X > 0 && v.Y > 0)
                {
                    o1 = o1 + v1.Normalize() * (Arena.goal_top_radius + Arena.goal_side_radius);

                    distance = distance.GetMinWith(
                                distance_to_sphere_outer(
                                    point,
                                    new Point3(o1.X, o1.Y, (Arena.depth / 2) + Arena.goal_side_radius),
                                    Arena.goal_side_radius));
                }
            }

            // Goal inside top corners
            if (point.Z > (Arena.depth / 2) + Arena.goal_side_radius &&
                point.Y > Arena.goal_height - Arena.goal_top_radius)
            {
                // Side x
                if (point.X > (Arena.goal_width / 2) - Arena.goal_top_radius)
                {
                    distance = distance.GetMinWith(distance_to_sphere_inner(
                point, new Point3((Arena.goal_width / 2) - Arena.goal_top_radius,
                Arena.goal_height - Arena.goal_top_radius, point.Z),
                Arena.goal_top_radius));
                }

                // Side z
                if (point.Z > (Arena.depth / 2) + Arena.goal_depth - Arena.goal_top_radius)
                {
                    distance = distance.GetMinWith(distance_to_sphere_inner(point,
                    new Point3(point.X, Arena.goal_height - Arena.goal_top_radius,
                    (Arena.depth / 2) + Arena.goal_depth - Arena.goal_top_radius),
                    Arena.goal_top_radius));
                }

                // Bottom corners
                if (point.Y < Arena.bottom_radius)
                {

                    // Side x
                    if (point.X > (Arena.width / 2) - Arena.bottom_radius)
                    {
                        distance = distance.GetMinWith(distance_to_sphere_inner(
                        point, new Point3((Arena.width / 2) - Arena.bottom_radius, Arena.bottom_radius, point.Z),
                        Arena.bottom_radius));

                        // Side z
                        if (point.Z > (Arena.depth / 2) - Arena.bottom_radius &&
                            point.X >= (Arena.goal_width / 2) + Arena.goal_side_radius)
                        {
                            distance = distance.GetMinWith(distance_to_sphere_inner(
                            point, new Point3(point.X, Arena.bottom_radius, (Arena.depth / 2) - Arena.bottom_radius),
                            Arena.bottom_radius));

                            // Side z (goal)
                            if (point.Z > (Arena.depth / 2) + Arena.goal_depth - Arena.bottom_radius)
                            {
                                distance = distance.GetMinWith(distance_to_sphere_inner(point, new Point3(point.X, Arena.bottom_radius,
                                (Arena.depth / 2) + Arena.goal_depth - Arena.bottom_radius),
                                Arena.bottom_radius));
                            }

                            // Goal outer corner
                            var o2 = new Point((Arena.goal_width / 2) + Arena.goal_side_radius, (Arena.depth / 2) + Arena.goal_side_radius);
                            var v2 = new Point(point.X, point.Z) - o2;
                            if (v.X < 0 && v.Y < 0 &&
                                v.Length < Arena.goal_side_radius + Arena.bottom_radius)
                            {
                                o2 = o2 + v.Normalize() * (Arena.goal_side_radius + Arena.bottom_radius);
                                distance = distance.GetMinWith(
                                    distance_to_sphere_inner(
                                            point,
                                            new Point3(o2.X, Arena.bottom_radius, o2.Y), Arena.bottom_radius));
                            }

                            // Side x (goal)
                            if (point.Z >= (Arena.depth / 2) + Arena.goal_side_radius &&
                                point.X > (Arena.goal_width / 2) - Arena.bottom_radius)
                            {
                                distance = distance.GetMinWith(distance_to_sphere_inner(point, new Point3((Arena.goal_width / 2) - Arena.bottom_radius,
                                Arena.bottom_radius, point.Z), Arena.bottom_radius));
                            }

                            // Corner
                            if (point.X > (Arena.width / 2) - Arena.corner_radius &&
                                point.Z > (Arena.depth / 2) - Arena.corner_radius)
                            {
                                var corner_o = new Point((Arena.width / 2) - Arena.corner_radius, (Arena.depth / 2) - Arena.corner_radius);
                                var n = new Point(point.X, point.Z) - corner_o;
                                var dist = n.Length;
                                if (dist > Arena.corner_radius - Arena.bottom_radius)
                                {
                                    n = n / dist;
                                    var o3 = corner_o + n * (Arena.corner_radius - Arena.bottom_radius);
                                    distance = distance.GetMinWith(distance_to_sphere_inner(point, new Point3(o3.X, Arena.bottom_radius, o3.Y), Arena.bottom_radius));
                                }
                            }

                            // Ceiling corners
                            if (point.Y > Arena.height - Arena.top_radius)
                            {
                                // Side x
                                if (point.X > (Arena.width / 2) - Arena.top_radius)
                                {
                                    distance = distance.GetMinWith(
                                        distance_to_sphere_inner(
                                            point,
                                            new Point3((Arena.width / 2) - Arena.top_radius, Arena.height - Arena.top_radius, point.Z),
                                            Arena.top_radius));
                                }
                                // Side z
                                if (point.Z > (Arena.depth / 2) - Arena.top_radius)
                                {
                                    distance = distance.GetMinWith(
                                                distance_to_sphere_inner(
                                                    point,
                                                    new Point3(point.X, Arena.height - Arena.top_radius, (Arena.depth / 2) - Arena.top_radius),
                                                    Arena.top_radius));
                                }

                                // Corner
                                if (point.X > (Arena.width / 2) - Arena.corner_radius &&
                                    point.Z > (Arena.depth / 2) - Arena.corner_radius)
                                {
                                    var corner_o = new Point(
                                    (Arena.width / 2) - Arena.corner_radius,
                                    (Arena.depth / 2) - Arena.corner_radius);
                                    var dv = new Point(point.X, point.Z) - corner_o;
                                    if (dv.Length > Arena.corner_radius - Arena.top_radius)
                                    {
                                        var n = dv.Normalize();
                                        var o4 = corner_o + n * (Arena.corner_radius - Arena.top_radius);
                                        distance = distance.GetMinWith(distance_to_sphere_inner(
                                        point,
                                        new Point3(o4.X, Arena.height - Arena.top_radius, o4.Y),
                                        Arena.top_radius));
                                    }
                                }
                                return distance;
                            }
                        }
                    }
                }
            }
            return distance;
        }

        public static DistNorm distance_to_arena(Point3 point, Arena Arena, double objRadius)
        {
            var testPoint = point.Clone();

            var negative_x = testPoint.X < 0;
            var negative_z = testPoint.Z < 0;
            if (negative_x)
                testPoint.X = -testPoint.X;
            if (negative_z)
                testPoint.Z = -testPoint.Z;
            var result = distance_to_arena_quarter(testPoint, Arena, objRadius);
            if (negative_x)
                result.Normal.X = -result.Normal.X;
            if (negative_z)
                result.Normal.Z = -result.Normal.Z;
            return result;
        }

        public static Point CalculateNextGroundTouch(WorldState worldState)
        {
            var nextBallGroundPos = worldState.BallRoute.FirstOrDefault(b => b.IsAcheavable);
            if (nextBallGroundPos == null)
                nextBallGroundPos = worldState.BallRoute.Last();

            var ballTouchPosition = nextBallGroundPos.Position.ToXZPoint2();

            var directionToGate = (new Vector2(0, MyStrategy.Constants.GameY) - ballTouchPosition).Normalize();
            var targetPosition = ballTouchPosition - directionToGate * worldState.MyBall.Radius / 3;

            worldState.GetCurrentRobot().TargetPos = targetPosition.ToXZPoint3();
            return targetPosition;
        }

    }
}
