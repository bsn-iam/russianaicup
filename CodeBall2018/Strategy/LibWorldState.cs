﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Com.CodeGame.CodeBall2018.DevKit.CSharpCgdk.Model;

namespace Com.CodeGame.CodeBall2018.DevKit.CSharpCgdk
{
    public class WorldState
    {
        public MyBall MyBall;
        public Dictionary<int, MyRobot> MyRobots;

        public int Id { get; }
        public bool IsRealValue { get; set; }
        public long CalculationTime { get; private set; }
        public int TickIndex { get; private set; }
        public int RobotId { get; }
        public Dictionary<int, MyNitroPack> MyNitroPacks { get; internal set; }
        public List<MyBall> BallRoute { get; internal set; }

        public WorldState(int currentRobotId, MyBall myBall, Dictionary<int, MyRobot> myRobots, Dictionary<int, MyNitroPack> myNitroPacks, int tickIndex)
        {
            Id = Guid.NewGuid().GetHashCode();
            this.MyBall = myBall;
            this.MyRobots = myRobots;
            this.MyNitroPacks = myNitroPacks;
            this.TickIndex = tickIndex;
            this.RobotId = currentRobotId;
        }

        internal WorldState Clone()
        {
            var newRobots = new Dictionary<int, MyRobot>();
            foreach (var robot in MyRobots.Values)
                newRobots.Add(robot.Id, robot.LazyClone());

            var newNitroPacks = new Dictionary<int, MyNitroPack>();
            foreach (var pack in MyNitroPacks.Values)
                newNitroPacks.Add(pack.Id, pack.LazyClone());

            return new WorldState(RobotId, MyBall.LazyClone(), newRobots, newNitroPacks, TickIndex);
        }

        internal MyRobot GetCurrentRobot() => MyRobots[RobotId];
    }
}
