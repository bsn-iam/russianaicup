﻿//using MyStrategy;

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;

namespace Com.CodeGame.CodeBall2018.DevKit.CSharpCgdk
{
    public static partial class Visualizer
    {
        public static List<long> StepTimeList = new List<long>();
        private static double WorldHeight = MyStrategy.Constants.GameY;
        private static double WorldWidth = MyStrategy.Constants.GameX;

        private static void CreateImage(WorldState worldState)
        {
            try
            {
                #region Configuration

                isXYStartOnTop = false;

                var imageMultipier = 50;

                Image = new Bitmap((int)WorldWidth * imageMultipier, (int)WorldHeight * imageMultipier);
                //Graphics.Clear(Color.WhiteSmoke);
                Graphics = Graphics.FromImage(Image);

                #endregion

                var robot = worldState.GetCurrentRobot();

                foreach (var unit in worldState.MyRobots.Values)
                    FillCircle(Color.Beige, unit.Position.ToXZPoint2() * imageMultipier, 100);

                #region BonusMap

                #endregion


                //Tree
                var tree = robot.Tree;
                if (tree != null)
                {
                    foreach (var line in tree.Lines.Values)
                        DrawLineForBranchSequence(Color.Blue, tree, line);
                    DrawLineForBranchSequence(Color.Green, tree, tree.BestLine, 3);
                }


                #region System

                // map ranges
                DrawLine(Color.Black, 1, 1, 1, WorldHeight - 1, 3);
                DrawLine(Color.Black, 1, WorldHeight - 1, WorldWidth - 1, WorldHeight - 1, 3);
                DrawLine(Color.Black, WorldWidth - 1, 1, WorldWidth - 1, WorldHeight - 1, 3);
                DrawLine(Color.Black, WorldWidth - 1, 1, 1, 1, 3);

                //for (var i = 0; i < WorldHeight; i = i + 100)
                //    DrawLine(Color.LightSlateGray, 0, i, WorldWidth, i);
                //for (var i = 0; i < WorldWidth; i = i + 100)
                //    DrawLine(Color.LightGray, i, 0, i, WorldHeight);


                foreach (var seg in SegmentsDrawQueue)
                {
                    var points = seg[0] as List<System.Drawing.Point>;
                    var pen = seg[1] as Pen;
                    float width = seg.Length > 2 ? Convert.ToSingle(seg[2]) : 0F;
                    for (var i = 1; i < points.Count; i++)
                        DrawLine(pen.Color, points[i].X, points[i].Y, points[i - 1].X, points[i - 1].Y, width);
                }
                #endregion

            }
            catch { }

        }

        private static void DrawLineForBranchSequence(Color color, Tree tree, TreeLine line, int width = 0)
        {
            foreach (var branchId in line.BranchIDs)
            {
                var branch = tree.BranchList[branchId];
                var lineEndColor = branch.IsLineEnd &&
                    branch.Id != tree.BestLine.BranchIDs.Last() ? 
                    Color.Gray : color;

                DrawLine(lineEndColor, branch.Route.First().ToXZPoint2(), branch.Route.Last().ToXZPoint2(), width);
            }
        }

    }


}
