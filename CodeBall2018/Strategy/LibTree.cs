﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Com.CodeGame.CodeBall2018.DevKit.CSharpCgdk
{
    public class Tree
    {
        public Dictionary<int, Branch> BranchList { get; private set; } = new Dictionary<int, Branch>();
        public Dictionary<int, TreeLine> Lines { get; private set; } = new Dictionary<int, TreeLine>();

        public double MaxTimeMsec { get; private set; }
        public int MaxLineLength { get; private set; }
        public int MaxBranchCount { get; private set; }
        public int BranchTickLength { get; private set; }
        public double GenerationTime { get; private set; }
        private Branch Root { get; }
        public TreeLine BestLine { get; private set; }
        public ISimulator Simulator { get; private set; }
        public CommandGenerator CommandGen { get; private set; } = new CommandGenerator();
        private int MaxUsedGuid = 0;

        public Tree (WorldState stateNow, ISimulator simulator, int maxLineLength, int maxBranchCount, double maxTimeMsec, int branchTickLength)
        {
            this.MaxBranchCount = maxBranchCount;
            this.MaxLineLength = maxLineLength;
            this.MaxTimeMsec = maxTimeMsec;
            this.BranchTickLength = branchTickLength;
            this.Root = new Branch(stateNow, CommandGen.ForceOrders.First(), GetNewGuid(), true);
            Root.FinalState = Root.InitialState;
            Simulator = simulator;
            BranchList.Add(Root.Id, Root);
            Lines.Add(Root.Id, new TreeLine(Root));
            Simulator.IntegrateAndGetReward(Root, 0);
        }

        public void Generate()
        {
            var timer = new Stopwatch();
            timer.Restart();

            while (BranchList.Count < MaxBranchCount
                && timer.Elapsed.TotalMilliseconds < MaxTimeMsec
                && GetCurrentMaxLineLength() < MaxLineLength)
            {
                var bestLine = ChooseBestLine();
                var branchForFork = BranchList[bestLine.BranchIDs.Last()];
                Fork(branchForFork);
            }
            timer.Stop();
            BestLine = ChooseBestLine();
            GenerationTime = timer.Elapsed.TotalMilliseconds;

            var maxLineLength = GetCurrentMaxLineLength();
        }

        private void Fork (Branch parentBranch)
        {
            var parentLine = Lines[parentBranch.Id];

            var smartOrders = CommandGen.CalculateSmartOrders(parentBranch.ForkState);

            foreach (var forceOrder in smartOrders)
            {
                var newId = GetNewGuid();
                var newBranch = new Branch(parentBranch.ForkState, forceOrder, newId);
                
                parentBranch.AddChild(newId);
                BranchList.Add(newId, newBranch);

                newBranch.SetParent(parentBranch.Id);
                Simulator.IntegrateAndGetReward(newBranch, BranchTickLength);

                var newLine = parentLine.Clone();
                newLine.Extend(newBranch);
                Lines.Add(newId, newLine);
            }
            Lines.Remove(parentBranch.Id);
        }

        internal Order GetBestNextOrder()
        {
            int nextBranchId = 0;
            foreach (var branchId in BestLine.BranchIDs)
                if (!BranchList[branchId].IsRoot)
                {
                    nextBranchId = branchId;
                    break;
                }
            if (nextBranchId == 0)
                return CommandGen.ForceOrders.First();
            return BranchList[nextBranchId].Command;
        }

        private int GetCurrentMaxLineLength()
        {
            int lineLength = 0;
            foreach (var line in Lines.Values)
            {
                var currentLineLength = line.LegsAmount;
                if (currentLineLength > lineLength)
                    lineLength = currentLineLength;
            }
            return lineLength - 1;
        }

        private TreeLine ChooseBestLine()
        {
            double maxAverageReward = 0;
            var bestLine = Lines.Values.First();
            foreach (var line in Lines.Values)
            {
                var averageReward = line.GetLineReward();
                if (averageReward > maxAverageReward)
                {
                    maxAverageReward = averageReward;
                    bestLine = line;
                }
            }
            return bestLine;
        }

        private int GetNewGuid()
        {
            MaxUsedGuid++;
            return MaxUsedGuid;
        }

    }

    // 1. Sdelat' «vetvleniye» stvola tekushchego dereva (vsegda v seredine), 
    //yesli srednyaya dlina vetvey potomkov men'she dliny stvola.
    //Inache pereyti k vyboru potomka dlya rekursivnogo vyzova na nem. YA mnogo tut eksperimentiroval — eto variant okazalsya nailuchshim.

    // 2. Vybrat' potomka tekushchego dereva dlya rekursivnogo vyzova na nem. Vybirayetsya potomok s maksimal'nym znacheniyem: 
    //<srednyaya dlina vetvey v dereve> * <tret'ya otsenochnaya funktsiya> / sqrt(summa dlin vsekh vetvey dereva). 
    //Deleniye nuzhno chtoby derevo ne vyrozhdalos': chem bol'she vremeni potracheno na issledovaniye vetvi, tem bol'she znamenatel' i men'she prioritet.

    //3. Osnovnaya otsenochnaya funktsiya, na osnove kotoroy reshalos' po kakoy vetvi my vse zhe poyedem (kakaya vetv' luchshe). 
    //V pervoy versii znacheniye etoy funktsii ravnyalos' summe otsenki dlya stvola i maksimal'noy otsenki potomka.Nagrada davalas' 
    //za priblizheniye k finishu — «vzyatiye tayla». Pozzhe eta funktsiya sil'no izmenilas'.
}
