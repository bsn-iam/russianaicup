﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Com.CodeGame.CodeBall2018.DevKit.CSharpCgdk
{
    public class ActionSelector
    {
        public BonusMapCalculator BMCalculator = new BonusMapCalculator();

        public Simulator Simulator { get; private set; }
        public BallisticSimulator BallSimulator { get; private set; } = new BallisticSimulator();
        public List<MyBall> BallRoute = new List<MyBall>();
        private Range2 WorldRange = new Range2(-MyStrategy.Constants.GameX / 2, MyStrategy.Constants.GameX / 2, -MyStrategy.Constants.GameY / 2, MyStrategy.Constants.GameY / 2);

        internal Order Generate(WorldState worldState)
        {
            if (Simulator == null)
                InitMaps();

            bool gameIsOver = MyDistanceHelpers.IsGoalScored(worldState);
            if (gameIsOver)
                return new Order();

            var robot = worldState.GetCurrentRobot();

            BallSimulator.ReCalculateBallistic(worldState, 150, 1);

            //if ((robot.TargetPos - robot.TargetPosPrev).Length < 0.1 &&
            //    (robot.Position - robot.TargetPos).Length < 10 &&
            //    !robot.Action.UseNitro &&
            //    robot.Action.JumpSpeed == 0)
            //    return robot.Action;


            var robotSimulationTime = 20 / MyStrategy.Constants.TeamSize - BallSimulator.SimulationTime;

            var tree = new Tree(worldState, Simulator, 1, 1000, robotSimulationTime, 1);
            tree.Generate();
            robot.Tree = tree;

            var order = tree.GetBestNextOrder();

            //if (robot.BallCollision)
            //    if (robot.Position.Z < worldState.MyBall.Position.Z)
            //    {
            //        order.JumpSpeed = MyStrategy.Constants.ROBOT_MAX_JUMP_SPEED;
            //    }
            //
            robot.Action = order;

            //if (order.JumpSpeed > Double.Epsilon)
            //    Console.WriteLine($"Chosen jump is {order.JumpSpeed}");

            return order;

        }

        private void InitMaps()
        {
            var attractionMap = new BallAttractionMap(WorldRange, MapType.Flat, MyStrategy.Constants.MapPointsAmount);
            attractionMap.SetIsDynamic(true).SetWeight(1).SetName("Attraction");
            BMCalculator.AddMap(attractionMap);

            var nitroMap = new MyNitroAttractionMap(WorldRange, MapType.Flat, MyStrategy.Constants.MapPointsAmount);
            nitroMap.SetIsDynamic(true).SetWeight(1).SetName("Nitro");
            BMCalculator.AddMap(nitroMap);

            Simulator = new Simulator(BMCalculator);
        }

    }
}