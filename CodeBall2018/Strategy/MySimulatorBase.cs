﻿using Com.CodeGame.CodeBall2018.DevKit.CSharpCgdk.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Com.CodeGame.CodeBall2018.DevKit.CSharpCgdk
{
    public interface ISimObject
    {
        Point3 Position { get; set; }
        Vector3 Speed { get; set; }
        double Radius { get; set; }
        double Mass { get; }
        double ArenaE { get; } //BALL_ArenaE // robot_ArenaE
        double RadiusChangeSpeed { get; set; } // = robot.Action.JumpSpeed
        bool ArenaCollision { get; set; }
    }

    public abstract class SimulatorBase
    {
        protected Constants Constants = MyStrategy.Constants;
        protected Arena Arena = MyStrategy.Constants.Arena;
        protected BonusMapCalculator BMCalculator;

        protected bool CollideSimObjects(ISimObject a, ISimObject b)
        {
            double penetration = MyDistanceHelpers.CalculatePenetration(a, b);
            if (penetration > 0)
            {
                var k_a = (1 / a.Mass) / ((1 / a.Mass) + (1 / b.Mass));
                var k_b = (1 / b.Mass) / ((1 / a.Mass) + (1 / b.Mass));
                var normal = (b.Position - a.Position).Normalize().toVector3();
                a.Position -= normal * penetration * k_a;
                b.Position += normal * penetration * k_b;
                var delta_speed = (b.Speed - a.Speed) * normal - b.RadiusChangeSpeed - a.RadiusChangeSpeed;
                if (delta_speed < 0)
                {
                    //var randomValue = new Random().NextDouble();
                    var randomValue = 0.5;
                    var impulse = normal * (1 + Constants.MIN_HIT_E + randomValue * (Constants.MAX_HIT_E - Constants.MIN_HIT_E)) * delta_speed;
                    a.Speed += impulse * k_a;
                    b.Speed -= impulse * k_b;
                }
                return true;
            }
            return false;
        }

        public void MoveMicroTick(ISimObject e, double dtSecMicro)
        {
            e.Speed = e.Speed.Clamp(Constants.MAX_ENTITY_SPEED);
            e.Position += e.Speed * dtSecMicro;
            e.Position.Y -= Constants.GRAVITY * dtSecMicro * dtSecMicro / 2;
            e.Speed.Y -= Constants.GRAVITY * dtSecMicro;
        }

        protected Vector3 CollideWithArena(ISimObject e)
        {
            bool isNearArena = true; // IsNearArena(e);

            if (!isNearArena)
                return new Vector3();

            var distNorm = MyDistanceHelpers.distance_to_arena(e.Position, Arena, e.Radius);
            var penetration = e.Radius - distNorm.Distance;

            e.ArenaCollision = penetration > 0;

            if (penetration > 0)
            {
                e.Position += distNorm.Normal * penetration;

                var speed = (e.Speed * distNorm.Normal) - e.RadiusChangeSpeed;
                if (speed < 0)
                {
                    e.Speed -= distNorm.Normal * (1 + e.ArenaE) * speed;
                    return distNorm.Normal;
                }
            }
            return new Vector3();
        }

        protected bool IsNearArena(ISimObject e)
        {
            var eps = 0.001;
            var radius = e.Radius;
            var radiusEps = 2 * radius + eps;

            double ArenaWidthX = Constants.Arena.width; //X (-max/2, max/2)
            double ArenaHalfWidthX = ArenaWidthX / 2; //X (-max/2, max/2)
            double ArenaHeightY = Constants.Arena.height; //Y (0, max)
            double ArenaDepthZ = Constants.Arena.depth; //Z  (-max/2, max/2)
            double ArenaHalfDepthZ = ArenaDepthZ / 2; //Z  (-max/2, max/2)

            var rangeNoCollision = new Range3(-ArenaHalfWidthX + radiusEps, ArenaHalfWidthX - radiusEps,
                                              radiusEps, ArenaHeightY - radiusEps,
                                              -ArenaHalfDepthZ + radiusEps, ArenaHalfDepthZ - radiusEps);

            return !e.Position.IsInRange(rangeNoCollision);
        }

    }
}
