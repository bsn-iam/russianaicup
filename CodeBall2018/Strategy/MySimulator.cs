﻿using Com.CodeGame.CodeBall2018.DevKit.CSharpCgdk.Model;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace Com.CodeGame.CodeBall2018.DevKit.CSharpCgdk
{
    public class Simulator : SimulatorBase, ISimulator
    {
        private CommandGenerator SimComGen;
        public Simulator(BonusMapCalculator bMCalculator)
        {
            BMCalculator = bMCalculator;
            SimComGen = new CommandGenerator();
        }

        public void IntegrateAndGetReward(Branch branch, int tickLength, int microTicksPerTick = 1)
        {
            var ballInitialCollisionFound = MyDistanceHelpers.AreInCollisionOrCloser(branch.InitialState.GetCurrentRobot(), branch.InitialState.MyBall);
            branch.InitialState.GetCurrentRobot().BallCollision = ballInitialCollisionFound;

            var finalWorldState = branch.InitialState.Clone();
            branch.FinalState = finalWorldState;
            branch.FinalState.BallRoute = branch.InitialState.BallRoute;

            var currentEndRobot = branch.FinalState.GetCurrentRobot();
            var currentStartRobot = branch.InitialState.GetCurrentRobot();
            currentEndRobot.Action = branch.Command;

            for (int i = 0; i < tickLength; i++)
                Tick(finalWorldState, microTicksPerTick);

            var ballFinalCollisionFound = finalWorldState.GetCurrentRobot().BallCollision;

            var decidedToPushNow = false;
            if (!ballInitialCollisionFound && ballFinalCollisionFound)
            {
                decidedToPushNow = MyDistanceHelpers.DoIWantToPush(currentEndRobot, branch.FinalState.MyBall);
                if (decidedToPushNow && branch.Command.JumpSpeed < Double.Epsilon)
                    branch.Command.JumpSpeed = MyStrategy.Constants.ROBOT_MAX_JUMP_SPEED;
            }
                    

            if (ballInitialCollisionFound || ballFinalCollisionFound)
            {
                var counter = 0;
                while (finalWorldState.GetCurrentRobot().BallCollision &&
                finalWorldState.GetCurrentRobot().Position.Z < finalWorldState.MyBall.Position.Z 
                && counter < 25)
                {
                    var currentBallRoute = GenerateBallRoute(finalWorldState);
                    var newTargetSpeed = SimComGen.CalculateTargetSpeed(currentEndRobot, currentBallRoute);
                    currentEndRobot.Action.TargetSpeed = newTargetSpeed;

                    Tick(finalWorldState, microTicksPerTick);
                    counter++;

                    //Console.WriteLine($"Counter {counter}, last collision {resultingBallRoute.Last()}, new speed {newTargetSpeed}.");
                }

#if DEBUG
                if (finalWorldState.GetCurrentRobot().Action.JumpSpeed < Double.Epsilon &&
                    !branch.IsRoot &&
                    decidedToPushNow)
                    throw new Exception("Shouldn't be zero jump here.");
#endif

                //Horizontal Ball Speed reward
                //////////////////////////////////
                var finalBall = finalWorldState.MyBall;
                var directionToGate = (new Point(0, MyStrategy.Constants.GameY) - finalBall.Position.ToXZPoint2()).Normalize();
                var ballFlightDirection = finalBall.Speed.ToXZPoint2().Normalize();

                var misdirection = (directionToGate - ballFlightDirection).Length / 2;  //[0-1]
                var pushReward = 1 - misdirection;  //[1-0] positive weight

                var pushWeight = 1;
                branch.RewardList.Add(new Reward("BallSpeed", pushReward, pushWeight));

                //branch.RewardValue += pushReward * pushWeight;
                //////////////////////////////////

                //Horizontal ball direction reward
                //////////////////////////////////
                var leftCornerDirection = (MyStrategy.Constants.LeftCorner - finalBall.Position.ToXZPoint2()).Normalize();
                var rightCornerDirection = (MyStrategy.Constants.RightCorner - finalBall.Position.ToXZPoint2()).Normalize();
                var currentDirection = finalBall.Speed.Normalize().ToXZVector2();

                var distLeft = (leftCornerDirection - currentDirection).Length;
                var distRight = (rightCornerDirection - currentDirection).Length;
                var distCorners = (leftCornerDirection - rightCornerDirection).Length;

                var flightToGateHorisont = distLeft < distCorners && distRight < distCorners;

                if (flightToGateHorisont)
                {
                    double horizonReward = 1;
                    var horizonWeight = 2;
                    branch.RewardList.Add(new Reward("BallGateDirection", horizonReward, horizonWeight));
                    //branch.RewardValue += horizonReward * horizonWeight;
                }
                //////////////////////////////////

                //Vertical ball direction reward
                //////////////////////////////////
                if (flightToGateHorisont)
                {
                    List<MyBall> resultingBallRoute = GenerateBallRoute(finalWorldState);
                    var arenaCollisionPoint = resultingBallRoute.Last();

                    //Console.WriteLine($"Jump {branch.Command.JumpSpeed}, touch in {arenaCollisionPoint.Position}");

                    if (arenaCollisionPoint.Position.Y < MyStrategy.Constants.Arena.goal_height - finalWorldState.MyBall.Radius / 2 &&
                        arenaCollisionPoint.Position.Z >= MyStrategy.Constants.Arena.depth * 0.9 / 2)
                    {
                        double verticalReward =1 - Math.Abs(MyStrategy.Constants.Arena.goal_height / 2 - arenaCollisionPoint.Position.Y) / MyStrategy.Constants.Arena.goal_height;
                        var verticalWeight = 50;
                        branch.RewardList.Add(new Reward("GoalReward", verticalReward, verticalWeight));
                        //branch.RewardValue += verticalReward * verticalWeight;

                        //Console.WriteLine($"Jump {branch.Command.JumpSpeed}, touch in {arenaCollisionPoint.Position}. Jump reward is {verticalReward * verticalWeight}");
                    }
                }
                //////////////////////////////////

                //timer.Stop();
                //Console.WriteLine($"BallCollisionTimeStep {timer.Elapsed.Milliseconds}, counter = {counter}");



            }
            var bmValue = BMCalculator.CalculateReward(currentEndRobot.Position.ToXZPoint2(), finalWorldState);
            branch.RewardList.Add(new Reward("BonusMap", bmValue, 1));
            //branch.RewardValue += BMCalculator.CalculateReward(currentEndRobot.Position.ToXZPoint2(), finalWorldState);
            branch.Route.Add(currentEndRobot.Position);

            //else
            //{
            //    if (branch.Command.JumpSpeed == MyStrategy.Constants.ROBOT_MAX_JUMP_SPEED)
            //        branch.Reward += 10;
            //}

        }

        private static List<MyBall> GenerateBallRoute(WorldState finalWorldState)
        {
            var jumpBallSimulator = new BallisticSimulator();
            jumpBallSimulator.CalculateTillGateCollision(finalWorldState, 100, 1);
            var resultingBallRoute = jumpBallSimulator.BallRoute;
            return resultingBallRoute;
        }

        public void Tick(WorldState worldState, int microTicksPerTick)
        {
            double dtSec = 1.0 / Constants.TICKS_PER_SECOND;
            double dtSecMicro = dtSec / microTicksPerTick;

            for (int i = 0; i <= microTicksPerTick - 1; i++)
                Update(worldState.MyRobots, worldState.MyBall, worldState.MyNitroPacks, dtSecMicro);

            UpdateNitroPackState(worldState);
        }

        private static void UpdateNitroPackState(WorldState worldState)
        {
            foreach (var pack in worldState.MyNitroPacks.Values)
            {
                if (pack.IsAlive)
                {
                    continue;
                }
                pack.RespawnTicks -= 1;
                if (pack.RespawnTicks == 0)
                {
                    pack.IsAlive = true;
                }
            }
        }

        private void Update(Dictionary<int, MyRobot> allRobots, MyBall ball, Dictionary<int, MyNitroPack> nitroPacks, double dtSecMicro)
        {
            //shuffle(robots)
            var ownRobots = new List<MyRobot>();
            foreach (var robot in allRobots.Values)
                if (robot.IsMine)
                    ownRobots.Add(robot);

            foreach (var robot in ownRobots)
            {
                UpdateRobotArenaTouch(dtSecMicro, robot);

                UpdateRobotNitro(dtSecMicro, robot);

                MoveMicroTick(robot, dtSecMicro);

                UpdateRobotRadius(robot);
            }

            MoveMicroTick(ball, dtSecMicro);

            //CollideRobotsWithRobots(robots);

            CollideRobotWithBallAndArena(ownRobots, ball);
            CollideWithArena(ball);

            //UpdateGoals(ball);

            UpdateNitroPacks(allRobots, nitroPacks);
        }

        private void UpdateGoals(MyBall ball)
        {
            if (Math.Abs(ball.Position.Z) > Arena.depth / 2 + ball.Radius)
            {
                //    goal_scored();
            }
        }

        private void UpdateNitroPacks(Dictionary<int, MyRobot> robots, Dictionary<int, MyNitroPack> nitroPacks)
        {
            foreach (var robot in robots.Values)
            {
                if (robot.NitroAmount == Constants.MAX_NITRO_AMOUNT)
                {
                    continue;
                }
                foreach (var pack in nitroPacks.Values)
                {
                    if (!pack.IsAlive)
                    {
                        continue;
                    }
                    if ((robot.Position - pack.Position).Length <= robot.Radius + pack.Radius)
                    {
                        robot.NitroAmount = Constants.MAX_NITRO_AMOUNT;
                        pack.IsAlive = false;
                        pack.RespawnTicks = Constants.NITRO_PACK_RESPAWN_TICKS;
                    }
                }
            }
        }

        private void CollideRobotWithBallAndArena(List<MyRobot> ownRobots, MyBall ball)
        {
            foreach (var robot in ownRobots)
            {
                bool ballCollision = CollideSimObjects(robot, ball);
                robot.BallCollision = ballCollision;

                var collision_normal = CollideWithArena(robot);
                if (collision_normal.LengthSq == 0)
                {
                    robot.Touch = false;
                }
                else
                {
                    robot.Touch = true;
                    robot.TouchNormal = collision_normal;
                }
            }
        }

        private void CollideRobotsWithRobots(Dictionary<int, MyRobot> robots)
        {
            for (int i = 1; i <= robots.Count; i++)
                for (int j = i + 1; j <= robots.Count; j++)
                    CollideSimObjects(robots[i], robots[j]);
        }

        private void UpdateRobotRadius(MyRobot robot)
        {
            robot.Radius = Constants.ROBOT_MIN_RADIUS + (Constants.ROBOT_MAX_RADIUS - Constants.ROBOT_MIN_RADIUS) * robot.Action.JumpSpeed / Constants.ROBOT_MAX_JUMP_SPEED;
            robot.RadiusChangeSpeed = robot.Action.JumpSpeed;
        }

        private void UpdateRobotNitro(double dtSec, MyRobot robot)
        {
            if (robot.Action.UseNitro)
            {
                var target_speed_change = (robot.Action.TargetSpeed - robot.Speed).Clamp(robot.NitroAmount * Constants.NITRO_POINT_VELOCITY_CHANGE);
                if (target_speed_change.LengthSq > 0)
                {
                    var accelerationVector = target_speed_change.Normalize() * Constants.ROBOT_NITRO_ACCELERATION;
                    var speed_change = (accelerationVector * dtSec).Clamp(target_speed_change.Length);
                    robot.Speed += speed_change;
                    robot.NitroAmount -= speed_change.Length / Constants.NITRO_POINT_VELOCITY_CHANGE;
                }
            }
        }

        private void UpdateRobotArenaTouch(double dtSec, MyRobot robot)
        {
            if (robot.Touch)
            {
                var target_speed = robot.Action.TargetSpeed.Clamp(Constants.ROBOT_MAX_GROUND_SPEED);
                target_speed -= robot.TouchNormal * (robot.TouchNormal * target_speed);
                var target_speed_change = target_speed - robot.Speed;

                if (target_speed_change.LengthSq > 0)
                {
                    var acceleration = Constants.ROBOT_ACCELERATION * Math.Max(0, robot.TouchNormal.Y);
                    var desiredSpeed = target_speed_change.Normalize() * acceleration * dtSec;
                    robot.Speed += desiredSpeed.Clamp(target_speed_change.Length);
                }
            }
        }
    }
}
