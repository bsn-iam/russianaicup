﻿namespace Com.CodeGame.CodeBall2018.DevKit.CSharpCgdk
{
    internal class MyNitroAttractionMap : BonusMap
    {
        private Range2 worldRange;
        private MapType flat;
        private int mapPointsAmount;

        public MyNitroAttractionMap(Range2 activeWorldRange, MapType mapType, int mapPointsAmount) : base(activeWorldRange, mapType, mapPointsAmount)
        {
        }

        public override double CalculateRewardInWorldPoint(Point point, WorldState worldState)
        {
            var reward = AddUnitPointCalculation(worldState.GetCurrentRobot().TargetPos.ToXZPoint2(), point, worldState.MyBall.Radius, 1, MyStrategy.Constants.GameX * 1.2, 2);

            return reward;
        }
    }
}