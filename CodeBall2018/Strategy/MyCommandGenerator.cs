﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Com.CodeGame.CodeBall2018.DevKit.CSharpCgdk
{
    public class CommandGenerator
    {
        public List<Order> ForceOrders { get; }

        public List<double> JumpSpeeds = new List<double>();

        private Trigonometry Trig;

        public CommandGenerator()
        {
            Trig = new Trigonometry();
            ForceOrders = CalculateOrderCircle();

            var maxJumpSpeed = MyStrategy.Constants.ROBOT_MAX_JUMP_SPEED;

            JumpSpeeds.Add(1 * maxJumpSpeed);
            JumpSpeeds.Add(0.25 * maxJumpSpeed);
            JumpSpeeds.Add(0.5 * maxJumpSpeed);
            JumpSpeeds.Add(0.75 * maxJumpSpeed);
        }

        private List<Order> CalculateOrderCircle()
        {
            var dAlpha = 20;
            var forceOrders = new List<Order>();
            var radius = MyStrategy.Constants.MAX_ENTITY_SPEED;
            for (var angleDeg = 0; angleDeg <= 360; angleDeg += dAlpha)
            {
                var vector = new Vector3(radius * Trig.SinD(angleDeg), 0, radius * Trig.CosD(angleDeg));
                forceOrders.Add(new Order(vector));
            }
            forceOrders.Add(new Order(new Vector3()));
            return forceOrders;
        }

        public List<Order> CalculateSmartOrders(WorldState worldState)
        {
            var forceOrders = new List<Order>();
            var robot = worldState.GetCurrentRobot();
            var ballRoute = worldState.BallRoute;

            var targetSpeed = CalculateTargetSpeed(robot, ballRoute);
            var toPush = MyDistanceHelpers.DoIWantToPush(robot, worldState.MyBall);
            var toNitro = IsTimeToNitro(worldState) &&
                targetSpeed.Length > MyStrategy.Constants.MAX_ENTITY_SPEED * 0.95;




            if (toPush)
            {
                foreach (var jump in JumpSpeeds)
                {
                    forceOrders.Add(new Order(targetSpeed * 0.5, jump));
                    forceOrders.Add(new Order(targetSpeed * 0.9, jump));
                    forceOrders.Add(new Order(targetSpeed * 1.0, jump));
                    forceOrders.Add(new Order(targetSpeed * 1.1, jump));
                    forceOrders.Add(new Order(targetSpeed * 1.5, jump));

                }
            }
            else
            {
                forceOrders.Add(new Order(targetSpeed));
                forceOrders.Add(new Order(targetSpeed.ToXZVector2().RotateCCW(5 * Trig.Pi / 180).ToXZVector3()));
                forceOrders.Add(new Order(targetSpeed.ToXZVector2().RotateCCW(10 * Trig.Pi / 180).ToXZVector3()));
                forceOrders.Add(new Order(targetSpeed.ToXZVector2().RotateCW(5 * Trig.Pi / 180).ToXZVector3()));
                forceOrders.Add(new Order(targetSpeed.ToXZVector2().RotateCW(10 * Trig.Pi / 180).ToXZVector3()));

                forceOrders.Add(new Order(targetSpeed.ToXZVector2().RotateCCW(5 * Trig.Pi / 180).ToXZVector3() * 0.8));
                forceOrders.Add(new Order(targetSpeed.ToXZVector2().RotateCCW(10 * Trig.Pi / 180).ToXZVector3() * 0.8));
                forceOrders.Add(new Order(targetSpeed.ToXZVector2().RotateCW(5 * Trig.Pi / 180).ToXZVector3() * 0.8));
                forceOrders.Add(new Order(targetSpeed.ToXZVector2().RotateCW(10 * Trig.Pi / 180).ToXZVector3() * 0.8));

                forceOrders.Add(new Order(targetSpeed.ToXZVector2().RotateCCW(5 * Trig.Pi / 180).ToXZVector3() * 1.2));
                forceOrders.Add(new Order(targetSpeed.ToXZVector2().RotateCCW(10 * Trig.Pi / 180).ToXZVector3() * 1.2));
                forceOrders.Add(new Order(targetSpeed.ToXZVector2().RotateCW(5 * Trig.Pi / 180).ToXZVector3() * 1.2));
                forceOrders.Add(new Order(targetSpeed.ToXZVector2().RotateCW(10 * Trig.Pi / 180).ToXZVector3() * 1.2));
            }


            if (toNitro)
                foreach (var order in forceOrders)
                    order.UseNitro = true;

            return forceOrders;
        }

        private bool IsTimeToNitro(WorldState worldState)
        {
            var currentRobot = worldState.GetCurrentRobot();
            if (currentRobot.NitroAmount == 0)
                return false;

            var enemyRobots = worldState.MyRobots.Values.Where(r => !r.IsMine);
            var enemyApproachKoeff = GetApproachcKoeff(worldState.MyBall, enemyRobots);

            var myRobots = worldState.MyRobots.Values.Where(r => r.IsMine);
            var myApproachKoeff = GetApproachcKoeff(worldState.MyBall, myRobots);

            var result = enemyApproachKoeff > myApproachKoeff * 0.9;

            return result;

        }

        private static double GetApproachcKoeff(MyBall myBall, IEnumerable<MyRobot> robots)
        {
            var result = Double.MinValue;
            foreach (var robot in robots)
            {
                var directionToBall = myBall.Position - robot.Position;
                var dist = directionToBall.Length;
                var robotSpeedNorm = robot.Speed.Normalize();
                var speedToBallKoeff = 1 - (directionToBall.Normalize() - robot.Speed.Normalize()).Length / 2;  //[1-0] greater - closer
                var koeff = speedToBallKoeff / dist;

                if (koeff > result)
                    result = koeff;
            }
            return result;
        }

        public Vector3 CalculateTargetSpeed(MyRobot robot, List<MyBall> ballRoute)
        {
            var requiredLine = (robot.TargetPos - robot.Position).toVector3();
            var requiredDirection = requiredLine.Normalize();

            var requiredTicksAmount = ballRoute.Count;

            for (var i = 0; i < ballRoute.Count; i++)
                if (ballRoute[i].IsAcheavable)
                {
                    requiredTicksAmount = i;
                    break;
                }

            var requiredSpeedPerTick = requiredLine.Length / requiredTicksAmount;
            var requiredSpeed = requiredSpeedPerTick * MyStrategy.Constants.TICKS_PER_SECOND;
            var targetSpeed = requiredDirection * Math.Min(requiredSpeed, MyStrategy.Constants.MAX_ENTITY_SPEED);
            return targetSpeed;
        }
    }
}
