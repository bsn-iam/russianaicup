﻿using System;
using Com.CodeGame.CodeBall2018.DevKit.CSharpCgdk.Model;

namespace Com.CodeGame.CodeBall2018.DevKit.CSharpCgdk
{
    public class MyRobot : ISimObject
    {


        public MyRobot(int id, bool isMine)
        {
            Id = id;
            IsMine = isMine;
        }

        public bool IsMine { get; private set; }
        public Vector3 Speed { get; set; }
        public Point3 Position { get;  set; }
        public double Radius { get; set; }
        public bool Touch { get; set; }
        public int Id { get; }
        public Tree Tree { get; internal set; }
        public double RadiusChangeSpeed { get; set; } = 0; //Simulator usage only
        public Order Action { get; internal set; } = new Order();
        public Vector3 TouchNormal { get; set; }
        public double Mass { get; } = MyStrategy.Constants.ROBOT_MASS;
        public double ArenaE { get; } = MyStrategy.Constants.ROBOT_ARENA_E;
        public double NitroAmount { get; internal set; } = 0;
        public Point3 TargetPos { get; internal set; } = new Point3();
        public bool BallCollision { get; internal set; }
        public bool ArenaCollision { get; set; }
        public Point3 TargetPosPrev { get; internal set; } = new Point3();

        public void Update(Robot serverRobot)
        {
            Speed = new Vector3(serverRobot.velocity_x, serverRobot.velocity_y, serverRobot.velocity_z);
            Position = new Point3(serverRobot.x, serverRobot.y, serverRobot.z);
            Radius = serverRobot.radius;
            Touch = serverRobot.touch;
            TouchNormal = new Vector3(
                serverRobot.touch_normal_x != null? (double)serverRobot.touch_normal_x: 0, 
                serverRobot.touch_normal_y != null? (double)serverRobot.touch_normal_y: 0, 
                serverRobot.touch_normal_z != null? (double)serverRobot.touch_normal_z: 0);
            NitroAmount = serverRobot.nitro_amount;

            if (!serverRobot.is_teammate)
                Action.TargetSpeed = new Vector3(serverRobot.velocity_x, serverRobot.velocity_y, serverRobot.velocity_z);
        }

        public void Update(Point3 newPos, Vector3 newSpeed)
        {
            Speed = newSpeed;
            Position = newPos;
        }

        internal MyRobot LazyClone()
        {
            var clone = new MyRobot(Id, IsMine);
            clone.Speed = Speed.Clone();
            clone.Position = Position.Clone();
            clone.Radius = Radius;
            clone.NitroAmount = NitroAmount;
            clone.Action = Action;
            clone.TouchNormal = TouchNormal;
            clone.Touch = Touch;
            clone.BallCollision = BallCollision;
            clone.TargetPos = TargetPos;
            clone.TargetPosPrev = TargetPosPrev;

            return clone;
        }
    }
}