﻿using System.Collections.Generic;
using Com.CodeGame.CodeBall2018.DevKit.CSharpCgdk.Model;

namespace Com.CodeGame.CodeBall2018.DevKit.CSharpCgdk
{
    public class Constants
    {
        public Arena Arena;
        public int MaxTickCount;

        public double GameX { get; }
        public double GameY { get; set; }
        public Point LeftCorner { get; }
        public Point RightCorner { get; }
        public int TeamSize { get; }
        public int MapPointsAmount { get; } = 60;
        public int MapTickIndex { get; internal set; } = 0;

        //distance/second
        public double ROBOT_MIN_RADIUS { get; }
        public double ROBOT_MAX_RADIUS { get; }
        public double ROBOT_MAX_JUMP_SPEED { get; }
        public double ROBOT_ACCELERATION { get; }
        public double ROBOT_NITRO_ACCELERATION { get; }
        public double ROBOT_MAX_GROUND_SPEED { get; }
        public double ROBOT_ARENA_E { get; }
        public double ROBOT_RADIUS { get; }
        public double ROBOT_MASS { get; }
        public int TICKS_PER_SECOND { get; }
        public int MICROTICKS_PER_TICK { get; }
        public int RESET_TICKS { get; }
        public double BALL_ARENA_E { get; }
        public double BALL_RADIUS { get; }
        public double BALL_MASS { get; }
        public double MIN_HIT_E { get; }
        public double MAX_HIT_E { get; }
        public double MAX_ENTITY_SPEED { get; }
        public double MAX_NITRO_AMOUNT { get; }
        public double START_NITRO_AMOUNT { get; }
        public double NITRO_POINT_VELOCITY_CHANGE { get; }
        public double NITRO_PACK_X { get; }
        public double NITRO_PACK_Y { get; }
        public double NITRO_PACK_Z { get; }
        public double NITRO_PACK_RADIUS { get; }
        public double NITRO_PACK_AMOUNT { get; }
        public int NITRO_PACK_RESPAWN_TICKS { get; }
        public double GRAVITY { get; }


        public Constants(Rules rules)
        {
            Arena = rules.arena;
            MaxTickCount = rules.max_tick_count;
            GameX = Arena.depth;
            GameY = Arena.width;
            LeftCorner = new Point(Arena.goal_width / 2, Arena.depth / 2);
            RightCorner = new Point(-Arena.goal_width / 2, Arena.depth / 2);

            //distance/second
            this.TeamSize                   = rules.team_size;
            this.ROBOT_MIN_RADIUS           = rules.ROBOT_MIN_RADIUS;
            this.ROBOT_MAX_RADIUS           = rules.ROBOT_MAX_RADIUS;
            this.ROBOT_MAX_JUMP_SPEED       = rules.ROBOT_MAX_JUMP_SPEED; // / rules.TICKS_PER_SECOND;
            this.ROBOT_ACCELERATION         = rules.ROBOT_ACCELERATION; // / rules.TICKS_PER_SECOND;
            this.ROBOT_NITRO_ACCELERATION   = rules.ROBOT_NITRO_ACCELERATION; // / rules.TICKS_PER_SECOND;
            this.ROBOT_MAX_GROUND_SPEED     = rules.ROBOT_MAX_GROUND_SPEED; // / rules.TICKS_PER_SECOND;
            this.ROBOT_ARENA_E              = rules.ROBOT_ARENA_E;
            this.ROBOT_RADIUS               = rules.ROBOT_RADIUS;
            this.ROBOT_MASS                 = rules.ROBOT_MASS;
            this.TICKS_PER_SECOND           = rules.TICKS_PER_SECOND;
            this.MICROTICKS_PER_TICK        = rules.MICROTICKS_PER_TICK;
            this.RESET_TICKS                = rules.RESET_TICKS;
            this.BALL_ARENA_E               = rules.BALL_ARENA_E;
            this.BALL_RADIUS                = rules.BALL_RADIUS;
            this.BALL_MASS                  = rules.BALL_MASS;
            this.MIN_HIT_E                  = rules.MIN_HIT_E;
            this.MAX_HIT_E                  = rules.MAX_HIT_E;
            this.MAX_ENTITY_SPEED           = rules.MAX_ENTITY_SPEED; // / rules.TICKS_PER_SECOND;
            this.MAX_NITRO_AMOUNT           = rules.MAX_NITRO_AMOUNT;
            this.START_NITRO_AMOUNT         = rules.START_NITRO_AMOUNT;
            this.NITRO_POINT_VELOCITY_CHANGE= rules.NITRO_POINT_VELOCITY_CHANGE; //?
            this.NITRO_PACK_X               = rules.NITRO_PACK_X; //?
            this.NITRO_PACK_Y               = rules.NITRO_PACK_Y; //?
            this.NITRO_PACK_Z               = rules.NITRO_PACK_Z; //?
            this.NITRO_PACK_RADIUS          = rules.NITRO_PACK_RADIUS;
            this.NITRO_PACK_AMOUNT          = rules.NITRO_PACK_AMOUNT;
            this.NITRO_PACK_RESPAWN_TICKS   = rules.NITRO_PACK_RESPAWN_TICKS;
            this.GRAVITY                    = rules.GRAVITY; // / rules.TICKS_PER_SECOND;

        }

        //public const int ticksPerSecond = 60;
        //public int MICROTICKS_PER_TICK { get; } = 100;
        //
        ////distance/second
        //public int TICKS_PER_SECOND { get; } = 60;
        //public int ROBOT_MIN_RADIUS { get; } = 1;
        //public double ROBOT_MAX_RADIUS { get; } = 1.05;
        //public int ROBOT_MAX_JUMP_SPEED { get; } = 15;
        //public int ROBOT_ACCELERATION { get; } = 100 / ticksPerSecond / ticksPerSecond;
        //public int ROBOT_NITRO_ACCELERATION { get; } = 30 / ticksPerSecond / ticksPerSecond;
        //public int ROBOT_MAX_GROUND_SPEED { get; } = 30 / ticksPerSecond * MICROTICKS_PER_TICK;
        //public int ROBOT_ARENA_E { get; } = 0;
        //public int ROBOT_RADIUS { get; } = 1;
        //public int ROBOT_MASS { get; } = 2;
        //
        //public int RESET_TICKS { get; } = 2 * ticksPerSecond;
        //public double BALL_ARENA_E { get; } = 0.7;
        //public int BALL_RADIUS { get; } = 2;
        //public int BALL_MASS { get; } = 1;
        //public double MIN_HIT_E { get; } = 0.4;
        //public double MAX_HIT_E { get; } = 0.5;
        //public int MAX_ENTITY_SPEED { get; } = 100 / ticksPerSecond;
        //public int MAX_NITRO_AMOUNT { get; } = 100;
        //public int START_NITRO_AMOUNT { get; } = 50;
        //public double NITRO_POINT_VELOCITY_CHANGE { get; } = 0.6 / ticksPerSecond;
        //public int NITRO_PACK_X { get; } = 20;
        //public int NITRO_PACK_Y { get; } = 1;
        //public int NITRO_PACK_Z { get; } = 30;
        //public double NITRO_PACK_RADIUS { get; } = 0.5;
        //public int NITRO_PACK_AMOUNT { get; } = 100;
        //public int NITRO_RESPAWN_TICKS { get; } = 10;
        //public int GRAVITY { get; } = 30 / ticksPerSecond / ticksPerSecond;


    }
}
