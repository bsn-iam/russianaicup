﻿using Com.CodeGame.CodeBall2018.DevKit.CSharpCgdk.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Com.CodeGame.CodeBall2018.DevKit.CSharpCgdk
{
    public class FullSimulator : SimulatorBase, ISimulator
    {
        public FullSimulator(BonusMapCalculator bMCalculator)
        {
            BMCalculator = bMCalculator;
        }

        public void IntegrateAndGetReward(Branch branch, int tickLength, int microTicksPerTick = 1) //Constants.TICKS_PER_SECOND
        {
            var finalWorldState = branch.InitialState.Clone();
            branch.FinalState = finalWorldState;
            branch.FinalState.BallRoute = branch.InitialState.BallRoute;

            var robot = branch.FinalState.GetCurrentRobot();
            robot.Action = branch.Command;

            for (int i = 0; i < tickLength; i++)
                Tick(finalWorldState, microTicksPerTick);


            var bmValue = BMCalculator.CalculateReward(robot.Position.ToXZPoint2(), finalWorldState);
            branch.RewardList.Add(new Reward("BonusMap", bmValue, 1));
            //branch.RewardValue += BMCalculator.CalculateReward(robot.Position.ToXZPoint2(), finalWorldState);
            branch.Route.Add(robot.Position);
        }

        public void Tick(WorldState worldState, int microTicksPerTick)
        {
            double dtSec = 1.0 / Constants.TICKS_PER_SECOND;
            double dtSecMicro = dtSec / microTicksPerTick;

            for (int i = 0; i <= microTicksPerTick - 1; i++)
                Update(worldState.MyRobots, worldState.MyBall, worldState.MyNitroPacks, dtSecMicro);

            UpdateNitroPackState(worldState);
        }

        private static void UpdateNitroPackState(WorldState worldState)
        {
            foreach (var pack in worldState.MyNitroPacks.Values)
            {
                if (pack.IsAlive)
                {
                    continue;
                }
                pack.RespawnTicks -= 1;
                if (pack.RespawnTicks == 0)
                {
                    pack.IsAlive = true;
                }
            }
        }

        private void Update(Dictionary<int, MyRobot> robots, MyBall ball, Dictionary<int, MyNitroPack> nitroPacks, double dtSecMicro)
        {
            //shuffle(robots)
            foreach (var robot in robots.Values)
            {
                UpdateRobotArenaTouch(dtSecMicro, robot);

                UpdateRobotNitro(dtSecMicro, robot);

                MoveMicroTick(robot, dtSecMicro);

                UpdateRobotRadius(robot);
            }

            MoveMicroTick(ball, dtSecMicro);

            CollideRobotsWithRobots(robots);

            CollideRobotWithBallAndArena(robots, ball);
            CollideWithArena(ball);

            //UpdateGoals(ball);

            UpdateNitroPacks(robots, nitroPacks);
        }

        private void UpdateGoals(MyBall ball)
        {
            if (Math.Abs(ball.Position.Z) > Arena.depth / 2 + ball.Radius)
            {
                //    goal_scored();
            }
        }

        private void UpdateNitroPacks(Dictionary<int, MyRobot> robots, Dictionary<int, MyNitroPack> nitroPacks)
        {
            foreach (var robot in robots.Values)
            {
                if (robot.NitroAmount == Constants.MAX_NITRO_AMOUNT)
                {
                    continue;
                }
                foreach (var pack in nitroPacks.Values)
                {
                    if (!pack.IsAlive)
                    {
                        continue;
                    }
                    if ((robot.Position - pack.Position).Length <= robot.Radius + pack.Radius)
                    {
                        robot.NitroAmount = Constants.MAX_NITRO_AMOUNT;
                        pack.IsAlive = false;
                        pack.RespawnTicks = Constants.NITRO_PACK_RESPAWN_TICKS;
                    }
                }
            }
        }

        private void CollideRobotWithBallAndArena(Dictionary<int, MyRobot> robots, MyBall ball)
        {
            foreach (var robot in robots.Values)
            {
                CollideSimObjects(robot, ball);

                var collision_normal = CollideWithArena(robot);
                if (collision_normal.LengthSq == 0)
                {
                    robot.Touch = false;
                }
                else
                {
                    robot.Touch = true;
                    robot.TouchNormal = collision_normal;
                }
            }
        }

        private void CollideRobotsWithRobots(Dictionary<int, MyRobot> robots)
        {
            for (int i = 1; i <= robots.Count; i++)
                for (int j = i + 1; j <= robots.Count; j++)
                    CollideSimObjects(robots[i], robots[j]);
        }

        private void UpdateRobotRadius(MyRobot robot)
        {
            robot.Radius = Constants.ROBOT_MIN_RADIUS + (Constants.ROBOT_MAX_RADIUS - Constants.ROBOT_MIN_RADIUS) * robot.Action.JumpSpeed / Constants.ROBOT_MAX_JUMP_SPEED;
            robot.RadiusChangeSpeed = robot.Action.JumpSpeed;
        }

        private void UpdateRobotNitro(double dtSec, MyRobot robot)
        {
            if (robot.Action.UseNitro)
            {
                var target_speed_change = (robot.Action.TargetSpeed - robot.Speed).Clamp(robot.NitroAmount * Constants.NITRO_POINT_VELOCITY_CHANGE);
                if (target_speed_change.LengthSq > 0)
                {
                    var accelerationVector = target_speed_change.Normalize() * Constants.ROBOT_NITRO_ACCELERATION;
                    var speed_change = (accelerationVector * dtSec).Clamp(target_speed_change.Length);
                    robot.Speed += speed_change;
                    robot.NitroAmount -= speed_change.Length / Constants.NITRO_POINT_VELOCITY_CHANGE;
                }
            }
        }

        private void UpdateRobotArenaTouch(double dtSec, MyRobot robot)
        {
            if (robot.Touch)
            {
                var target_speed = robot.Action.TargetSpeed.Clamp(Constants.ROBOT_MAX_GROUND_SPEED);
                target_speed -= robot.TouchNormal * (robot.TouchNormal * target_speed);
                var target_speed_change = target_speed - robot.Speed;

                if (target_speed_change.LengthSq > 0)
                {
                    var acceleration = Constants.ROBOT_ACCELERATION * Math.Max(0, robot.TouchNormal.Y);
                    var desiredSpeed = target_speed_change.Normalize() * acceleration * dtSec;
                    robot.Speed += desiredSpeed.Clamp(target_speed_change.Length);
                }
            }
        }
    }
}
