﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;

namespace Com.CodeGame.CodeBall2018.DevKit.CSharpCgdk
{
    internal class OutputGenerator
    {
        private Approximator TreeTimeApproximator;
        private Approximator BallTimeApproximator;
        private Approximator BallTestTimeApproximator;
        private JArray Result;

        public OutputGenerator()
        {
            TreeTimeApproximator = new Approximator(50);
            BallTimeApproximator = new Approximator(5);
            BallTestTimeApproximator = new Approximator(5);
        }

        internal string GenerateOutput(WorldState worldState)
        {
            Result = new JArray();

            bool gameIsOver = MyDistanceHelpers.IsGoalScored(worldState);
            if (gameIsOver)
                return Result.ToString();

            var treeTime = worldState.GetCurrentRobot().Tree.GenerationTime;
            TreeTimeApproximator.AddValue(treeTime);
            AddText($"TreeTime, mc {TreeTimeApproximator.CalculateAverage():f2}");

            BallTimeApproximator.AddValue(MyStrategy.ActionSelector.BallSimulator.SimulationTime);
            AddText($"BallTime, mc {BallTimeApproximator.CalculateAverage():f2}/{MyStrategy.ActionSelector.BallSimulator.MaxSimulationTime:f2}");

            AddText($"BranchCount {worldState.GetCurrentRobot().Tree.BranchList.Count - 1}");

            var totalTickTime = 0.0;
            foreach (var timer in MyStrategy.OrderGenerationTimers)
            {
                var currentTime = timer.Value.CalculateAverage();
                totalTickTime += currentTime;
                AddText($"Spent {timer.Key} - {currentTime:f2} mc");
            }

            AddText($"Spent total {totalTickTime:f2} mc");
            AddText($"Spent average {MyStrategy.TotalTimeAverage:f2} mc");


            var robot = worldState.GetCurrentRobot();
            var tree = robot.Tree;


            //Robot Orders
            foreach (var unit in worldState.MyRobots.Values)
            {
                AddLine(unit.Position, unit.Position + unit.Speed, 0.5);
                AddLine(unit.Position, unit.Position + unit.Action.TargetSpeed, 1.5);

                if (unit.IsMine && unit.TargetPos != null)
                    AddSphere(unit.TargetPos, 0.5);
            }

            //ballPath
            Point3 ballLineStart = new Point3();
            foreach (var ball in worldState.BallRoute)
            {
                if (!ballLineStart.IsEmpty())
                {
                    AddSphere(ball.Position, 0.1);
                    AddLine(ballLineStart, ball.Position, 0.1);
                }
                ballLineStart = ball.Position.Clone();
            }

            AddSphere(MyDistanceHelpers.CalculateNextGroundTouch(worldState).ToXZPoint3(), 0.5);


            //Robot Trees

            foreach (var unit in worldState.MyRobots.Values)
            {
                if (unit.Tree == null)
                    break;
                var treeHeight = new Vector3(0, unit.Radius, 0);
                var branches = unit.Tree.BranchList.Values;
                foreach(var branch in branches)
                {
                    AddLine(branch.InitialState.GetCurrentRobot().Position + treeHeight, branch.FinalState.GetCurrentRobot().Position + treeHeight, 0.2);
                }
            }


            return Result.ToString();
        }


        private void AddText(string text)
        {
            var textJson = JObject.FromObject(new
            {
                Text = text
            });
            Result.Add(textJson);
        }

        private void AddSphere(Point3 p, double r)
        {
            var sphereJson = JObject.FromObject(new
            {
                Sphere = new
                {
                    x = p.X,
                    y = p.Y,
                    z = p.Z,
                    radius = r,
                    r = 1,
                    g = 1,
                    b = 1,
                    a = 1
                }
            });

            Result.Add(sphereJson);
        }


        private void AddLine(Point3 a, Point3 b, double w)
        {
            var lineJson = JObject.FromObject(new
            {
                Line = new
                {
                    x1 = a.X,
                    y1 = a.Y,
                    z1 = a.Z,
                    x2 = b.X,
                    y2 = b.Y,
                    z2 = b.Z,
                    width = w,
                    r = 1,
                    g = 1,
                    b = 1,
                    a = 1
                }
            });
            Result.Add(lineJson);
        }

        //[
        //  {
        //    "Sphere": {
        //      "x": 2.0,
        //      "y": 5.0,
        //      "z": 15.0,
        //      "radius": 0.1,
        //      "r": 1.0,
        //      "g": 0.0,
        //      "b": 0.0,
        //      "a": 0.5
        //    }
        //},
        //  {
        //    "Text": "Debug text #0"
        //  },
        //  {
        //    "Line": {
        //      "x1": 0.0,
        //      "y1": 0.0,
        //      "z1": 0.0,
        //      "x2": 10.0,
        //      "y2": 20.0,
        //      "z2": 30.0,
        //       "width": 1.0,
        //       "r": 1.0,
        //       "g": 1.0,
        //       "b": 1.0,
        //       "a": 1.0
        //    }
        //  }
        //]
    }
}