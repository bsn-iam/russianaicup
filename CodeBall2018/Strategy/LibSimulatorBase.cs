﻿namespace Com.CodeGame.CodeBall2018.DevKit.CSharpCgdk
{
    public interface ISimulator
    {
        void IntegrateAndGetReward(Branch branch, int tickLength, int microTicksPerTick = 1);
    }
}