﻿using System;
using System.Linq;

namespace Com.CodeGame.CodeBall2018.DevKit.CSharpCgdk
{
    internal class BallAttractionMap: BonusMap
    {
        private Range2 worldRange;
        private MapType flat;
        private int mapPointsAmount;

        public BallAttractionMap(Range2 activeWorldRange, MapType flat, int mapPointsAmount) : base(activeWorldRange, MapType.Final, mapPointsAmount)
        {
        }

        public override double CalculateRewardInWorldPoint(Point point, WorldState worldState)
        {
            double reward = 0;
            foreach (var pack in worldState.MyNitroPacks.Values)
                reward += AddUnitPointCalculation(pack.Position.ToXZPoint2(), point, pack.Radius, 1, 5, 2);

            return reward;
        }


    }
}