﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Com.CodeGame.CodeBall2018.DevKit.CSharpCgdk
{
    public class Approximator
    {
        private int HistoryLength = 0;
        private List<double> History = new List<double>();

        public Approximator(int historyLength)
        {
            HistoryLength = historyLength;
        }

        public void AddValue(double value)
        {
            History.Add(value);
            if (History.Count > HistoryLength)
                History.RemoveAt(0);
        }

        public double CalculateAverage() => History.Average();
    }
}
