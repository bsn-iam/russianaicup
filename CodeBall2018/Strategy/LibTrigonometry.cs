﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.CodeGame.CodeBall2018.DevKit.CSharpCgdk
{
    public class Trigonometry
    {
        private Dictionary<int, double> SinTable = new Dictionary<int, double>();
        private Dictionary<int, double> CosTable = new Dictionary<int, double>();
        public double Pi { get; } = Math.PI;

        public Trigonometry()
        {
            for (int i = 0; i <= 360; i+=5)
            {
                SinTable.Add(i, Math.Sin(i * Pi / 180));
                CosTable.Add(i, Math.Cos(i * Pi / 180));
            }
        }

        public double SinD (int angleDeg) =>  SinTable[angleDeg];
        public double CosD(int angleDeg) => CosTable[angleDeg];

        public double Sin (double angleDeg)
        {
            var minAngleDeg = (int)angleDeg;
            var maxAngleDeg = minAngleDeg + 1;

            var sinMin = SinD(minAngleDeg);
            var sinMax = SinD(maxAngleDeg);

            var result = sinMin + (sinMax - sinMin) * (angleDeg - minAngleDeg);

            return result;
        }

        public double Sin (Angle angle) => Sin(angle.ValueInDegrees);
    }
}
