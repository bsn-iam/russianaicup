﻿using Com.CodeGame.CodeBall2018.DevKit.CSharpCgdk.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Com.CodeGame.CodeBall2018.DevKit.CSharpCgdk
{
    public class MyBall: ISimObject
    {

        public Vector3 Speed { get; set; } = new Vector3();
        public Point3 Position { get; set; } = new Point3();
        public double Radius { get; set; } = MyStrategy.Constants.BALL_RADIUS;
        private double RobotMiddleRadius = MyStrategy.Constants.ROBOT_RADIUS + 0.5 * (MyStrategy.Constants.ROBOT_MAX_RADIUS - MyStrategy.Constants.ROBOT_RADIUS);
        public double Mass { get; } = MyStrategy.Constants.BALL_MASS;
        public double ArenaE { get; } = MyStrategy.Constants.BALL_ARENA_E;
        public double RadiusChangeSpeed { get; set; } = 0;
        public bool IsAcheavable { get; internal set; } = false;
        public bool ArenaCollision { get; set; }

        private const double Eps = 1e-10;

        public void Update (Ball ball)
        {
            var newSpeed = new Vector3(ball.velocity_x, ball.velocity_y, ball.velocity_z);
            TestForMaxEntitySpeedDebug(ball);
            Speed = newSpeed;
            Position = new Point3(ball.x, ball.y, ball.z);
        }

        public MyBall SetIsAcheavable()
        {
            IsAcheavable = Position.Y < Radius + RobotMiddleRadius;
            return this;
        }

        private void TestForMaxEntitySpeedDebug(Ball ball)
        {
#if DEBUG
            //string warning = "";
            if (Speed.LengthSq > Eps && Math.Abs(ball.z) > Eps)
                if (Speed.Length > MyStrategy.Constants.MAX_ENTITY_SPEED)
                    throw new Exception($"Expected max speed is {MyStrategy.Constants.MAX_ENTITY_SPEED:f4}, real one - {Speed.Length}.");
            //warning = $"Expected max speed is {MyStrategy.Constants.MAX_ENTITY_SPEED:f4}, real one - {Speed.Length}.";
#endif
        }

        internal MyBall LazyClone()
        {
            var clone = new MyBall();
            clone.Speed = Speed.Clone();
            clone.Position = Position.Clone();

            return clone;
        }
    }
}
