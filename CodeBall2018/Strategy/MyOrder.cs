﻿using System;

namespace Com.CodeGame.CodeBall2018.DevKit.CSharpCgdk
{ 
    public class Order
    {
        public Vector3 TargetSpeed { get; set; } = new Vector3();
        public double JumpSpeed { get; set; } = 0;
        public bool UseNitro { get; set; } = false;
        public Order()
        {
        }

        public Order(Vector3 speed)
        {
            this.TargetSpeed = speed;
        }

        public Order(Vector3 speed, double jumpSpeed)
        {
            this.TargetSpeed = speed;
            JumpSpeed = jumpSpeed;
        }

        internal Order Clone()
        {
            var clone = new Order();
            clone.TargetSpeed = TargetSpeed.Clone();
            clone.JumpSpeed = JumpSpeed;
            clone.UseNitro = UseNitro;
            return clone;
        }
    }
}
