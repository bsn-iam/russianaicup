﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace Com.CodeGame.CodeBall2018.DevKit.CSharpCgdk
{
    public class BallisticSimulator : SimulatorBase
    {
        public double SimulationTime = 0;
        public double MaxSimulationTime = 0;
        public List<MyBall> BallRoute { get; private set; } = new List<MyBall>();
        private double EpsSq = 0.001;

        public void ReCalculateBallistic(WorldState startWorldState, int ticks, int microTicksPerTick)
        {
            var timer = new Stopwatch();
            timer.Start();

            ClearOldDataBeforeCurrent(startWorldState);

            var routeLength = BallRoute.Count;

            var ballNow = BallRoute.Last();

#if DEBUG
            var ballClearTimeWarning = "";
            if (timer.Elapsed.TotalMilliseconds > 2)
            {
                //timer.Stop();
                var clearTime = timer.Elapsed.TotalMilliseconds;
                //throw new Exception("Ball simulation time is huge!");
                ballClearTimeWarning = $"Ball Clear route time is {clearTime:f4}!";
                Console.WriteLine(ballClearTimeWarning);
            }
#endif

            for (int i = 0; i < ticks - routeLength; i++)
            {
                ballNow = Tick(ballNow.LazyClone(), microTicksPerTick).SetIsAcheavable();
                BallRoute.Add(ballNow);

                if (timer.Elapsed.TotalMilliseconds > 2)
                {
                    //timer.Stop();
                    break;
                }
            }

            startWorldState.BallRoute = BallRoute;
            var currentRobot = startWorldState.GetCurrentRobot();
            currentRobot.TargetPosPrev = currentRobot.TargetPos;
            currentRobot.TargetPos = MyDistanceHelpers.CalculateNextGroundTouch(startWorldState).ToXZPoint3();

            timer.Stop();
            UpdateSimulationTime(timer);

#if DEBUG
            var ballRouteTimeWarning = "";
            if (timer.Elapsed.TotalMilliseconds > 10)
            {
                var clearTime = timer.Elapsed.TotalMilliseconds;
                //throw new Exception("Ball simulation time is huge!");
                ballRouteTimeWarning = $"Ball Clear route time is {clearTime:f4}!";
                Console.WriteLine(ballRouteTimeWarning);
            }

#endif

            DebugTestDistancesInRoute(microTicksPerTick);

        }

        private void UpdateSimulationTime(Stopwatch timer)
        {
            SimulationTime = timer.Elapsed.TotalMilliseconds;
            if (MaxSimulationTime < SimulationTime)
                MaxSimulationTime = SimulationTime;
        }

        internal void CalculateTillGateCollision(WorldState worldState, int maxTicks, int microTicksPerTick)
        {
            var timer = new Stopwatch();
            timer.Start();

            BallRoute = new List<MyBall>();

            var ballNow = worldState.MyBall;
            BallRoute.Add(ballNow.LazyClone());

            for (int i = 0; i < maxTicks; i++)
            {
                ballNow = Tick(ballNow.LazyClone(), microTicksPerTick).SetIsAcheavable();
                BallRoute.Add(ballNow);

                if (timer.Elapsed.TotalMilliseconds > 10 ||
                    //ballNow.ArenaCollision)
                    ballNow.Position.Z > 0.95*(MyStrategy.Constants.Arena.depth/2-ballNow.Radius))
                {
                    break;
                }
            }

            timer.Stop();
            UpdateSimulationTime(timer);
        }

        private void DebugTestDistancesInRoute(int microTicksPerTick)
        {
#if DEBUG
            for (var i = 1; i < BallRoute.Count; i++)
            {
                var stepDistance = (BallRoute[i].Position - BallRoute[i - 1].Position).Length;
                if (stepDistance > Constants.MAX_ENTITY_SPEED / microTicksPerTick)
                    throw new Exception("Wrong Ball route calculation.");
            }
#endif
        }

        private void ClearOldDataBeforeCurrent(WorldState startWorldState)
        {
            if (BallRoute.Any())
            {
                int routePositionNow = GetBallIdInRoute(startWorldState);

                if (routePositionNow == BallRoute.Count - 1)
                {
                    //Position in route is not found.
                    BallRoute = new List<MyBall>();
                    BallRoute.Add(startWorldState.MyBall.LazyClone().SetIsAcheavable());
                }
                else
                {
                    //Remove outdated positions
                    for (var i = 0; i < routePositionNow; i++)
                        BallRoute.RemoveAt(0);
                }
            }
            else
            {
                BallRoute.Add(startWorldState.MyBall.LazyClone().SetIsAcheavable());
            }
        }

        private int GetBallIdInRoute(WorldState startWorldState)
        {
            var ballReal = startWorldState.MyBall;
            int routePositionNow = BallRoute.Count - 1;

            //Get ball position in trajectory
            for (var i = 0; i < BallRoute.Count; i++)
            {
                var routeBall = BallRoute[i];
                if ((routeBall.Position - ballReal.Position).LengthSq < EpsSq)
                {
                    routePositionNow = i;
                    break;
                }
            }

            return routePositionNow;
        }

        public MyBall Tick(MyBall ball, int microTicksPerTick)
        {
            double dtSec = 1.0 / Constants.TICKS_PER_SECOND;
            double dtSecMicro = dtSec / microTicksPerTick;

            var timer = new Stopwatch();
            timer.Start();

            for (int i = 0; i <= microTicksPerTick - 1; i++)
            {
                MoveMicroTick(ball, dtSecMicro);
                CollideWithArena(ball);

                if (timer.Elapsed.TotalMilliseconds > 2)
                {
                    timer.Stop();
                    Console.WriteLine($"Tick time greater 2 mc. Now {timer.Elapsed.TotalMilliseconds:f4}");
                }
            }

            return ball;
        }

    }
}
